[![Get it on Google Play](https://play.google.com/intl/en_gb/badges/images/generic/en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.damsky.danny.weatheronsteroids)

### What is this repository for? ###

Weather on Steroids is a FOSS Weather application for Android with Google Maps integration.
The application supports Android Lollipop and above.

Weather on Steroids is written in Java 8 using the following technologies:


*  Android Architecture Components - Room, ViewModel, LiveData, Paging and WorkManager.
*  Firebase Libraries - Firestore, Crashlytics, Authentication, Firebase-UI (for authentication)
*  Google Services Libraries - Maps SDK, Maps Utils, Places SDK
*  [Gson](https://github.com/google/gson)
*  [OkHttpClient](http://square.github.io/okhttp/)
*  [RxJava 2](https://github.com/ReactiveX/RxJava)
*  [Butterknife](http://jakewharton.github.io/butterknife/)
*  [Glide](https://github.com/bumptech/glide)
*  [FancyToast](https://github.com/Shashank02051997/FancyToast-Android)
*  [Speed Dial](https://github.com/leinardi/FloatingActionButtonSpeedDial)
*  [Facebook Year Class](https://github.com/facebook/device-year-class)

This repository is intended allow other people to learn to use the above technologies as well
as help others implement a better weather application for all of us to enjoy.

### How do I get set up? ###

Do a checkout of the repository in Android Studio.


Add your own google-services.json file into the app folder (You gotta register your forked app in Firebase and
download the JSON from there)


Also replace "YOUR_KEY_HERE" with your own API key for Google Maps and Places. (There are only two places
where you have to do this, the first one is in the AndroidManifest.xml file and the other one is in
.utils.PlaceUtils class)

### Contribution guidelines ###

If you want to contribute to the project, there are quite a few features that I wish to add,
and bug fixes are always welcome.
Feel free to e-mail me at dannydamsky99@gmail.com to tell me if you have an idea for a feature, or if you
wanna help in other ways (Like translating the app to other languages)

### Who do I talk to? ###

My name: Danny Damsky 


E-mail: dannydamsky99@gmail.com


LinkedIn: https://www.linkedin.com/in/danny-damsky/

### LICENSE ###

   Copyright 2018 Danny Damsky

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.