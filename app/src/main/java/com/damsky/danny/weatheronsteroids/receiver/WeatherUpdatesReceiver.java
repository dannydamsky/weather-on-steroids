package com.damsky.danny.weatheronsteroids.receiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.work.DbOpsData;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;

import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

/**
 * This receiver listens for when the device boots up and for internal calls with a private action.
 * (Internal calls are made when there's a change in the SharedPreferences of the device regarding
 * auto updates and update schedule).
 * <br /> <br />
 * This receiver is in charge of starting periodic work for weather updates.
 *
 * @author Danny Damsky
 */
public final class WeatherUpdatesReceiver extends BroadcastReceiver {

    private static final String TAG =
            "com.damsky.danny.weatheronsteroids.receiver.WeatherUpdatesReceiver.TAG";
    private static final String ACTION_RESET =
            "com.damsky.danny.weatheronsteroids.receiver.WeatherUpdatesReceiver.ACTION_RESET";

    public static void notifyDataChanged(@NonNull Context context) {
        final Intent intent = new Intent(context, WeatherUpdatesReceiver.class);
        intent.setAction(ACTION_RESET);
        context.sendBroadcast(intent);
    }


    /**
     * Actions are checked under {@link #initWorker(Intent)}
     */
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        if (PreferencesHelper.getInstance().isAutoUpdateEnabled()) {
            initWorker(intent);
        } else {
            WorkManager.getInstance().cancelUniqueWork(TAG);
        }
    }

    private void initWorker(@NonNull Intent intent) {
        final String action = intent.getAction();
        if (action != null && (action.equals(ACTION_RESET) || action.equals(Intent.ACTION_BOOT_COMPLETED))) {
            final PeriodicWorkRequest request = getWorkRequest();
            WorkManager.getInstance().enqueueUniquePeriodicWork(TAG,
                    ExistingPeriodicWorkPolicy.REPLACE, request);
        }
    }

    @NonNull
    private PeriodicWorkRequest getWorkRequest() {
        return new PeriodicWorkRequest.Builder(DbOpsWorker.class,
                PreferencesHelper.getInstance().getUpdateScheduleInHours(), TimeUnit.HOURS)
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_UPDATE_ALL)
                        .buildData())
                .setConstraints(getConstraints())
                .build();
    }

    @NonNull
    private Constraints getConstraints() {
        return new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build();
    }
}
