package com.damsky.danny.weatheronsteroids.util;

import android.net.Uri;
import android.os.Build;

/**
 * Application constants class
 *
 * @author Danny Damsky
 */
public final class Constants {

    // Request codes
    public static final int REQUEST_CODE_PLACE_PICKER = 301;
    public static final int REQUEST_CODE_LOG_IN = 789;
    public static final int REQUEST_CODE_LOCATION_PERMISSION = 575;
    public static final int REQUEST_CODE_PLACES_SEARCH = 833;
    public static final int REQUEST_CODE_WRITE_STORAGE_PERMISSION = 743;

    // A constant used to determine device performance
    public static int DEVICE_YEAR;

    // URL constants
    public static final Uri URL_YAHOO = Uri.parse("https://www.yahoo.com/news/weather");
    public static final Uri URL_SOURCE_CODE = Uri.parse("https://bitbucket.org/dannydamsky/weather-on-steroids");
    public static final Uri URL_LINKED_IN = Uri.parse("https://www.linkedin.com/in/danny-damsky/");
    public static final Uri URL_STORE = Uri.parse("market://details?id=com.damsky.danny.weatheronsteroids");
    public static final Uri URL_STORE_BACKUP = Uri.parse("https://play.google.com/store/apps/details?id=com.damsky.danny.weatheronsteroids");
    public static final Uri URL_EMAIL = Uri.parse("mailto:dannydamsky99@gmail.com");

    // Intent and Bundle extras
    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_LOCATION_NAME = "extra_location_name";
    public static final String EXTRA_TAG = "extra_tag";
    public static final String EXTRA_URL = "extra_url";
    public static final String EXTRA_IS_LARGE_LAYOUT = "extra_is_large_layout";

    // Runtime information
    public static final boolean IS_MARSHMALLOW_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;

    private Constants() {
    }
}
