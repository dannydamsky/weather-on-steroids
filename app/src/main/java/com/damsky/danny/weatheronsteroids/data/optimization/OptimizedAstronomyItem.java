package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.weather.model.YahooAstronomy;
import com.damsky.danny.weatheronsteroids.util.TimeUtils;

import java.util.Objects;

/**
 * This class is contained inside of {@link OptimizedLocationItem}.
 * It contains information about the sunset and sunrise times.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see YahooAstronomy
 * @see TimeUtils
 */
public final class OptimizedAstronomyItem {

    private final String sunriseTime;
    private final String sunsetTime;

    OptimizedAstronomyItem(@NonNull YahooAstronomy astronomy, boolean isMetric) {
        if (isMetric) {
            this.sunriseTime = TimeUtils.convertTwelveHourToTwentyFourHourTime(astronomy.getSunriseTime());
            this.sunsetTime = TimeUtils.convertTwelveHourToTwentyFourHourTime(astronomy.getSunsetTime());
        } else {
            this.sunriseTime = TimeUtils.fixYahooTwelveHourTimeString(astronomy.getSunriseTime());
            this.sunsetTime = TimeUtils.fixYahooTwelveHourTimeString(astronomy.getSunsetTime());
        }
    }

    @NonNull
    public String getSunriseTime() {
        return sunriseTime;
    }

    @NonNull
    public String getSunsetTime() {
        return sunsetTime;
    }

    @Override
    public String toString() {
        return "OptimizedAstronomyItem{" +
                "sunriseTime='" + sunriseTime + '\'' +
                ", sunsetTime='" + sunsetTime + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedAstronomyItem)) return false;
        final OptimizedAstronomyItem that = (OptimizedAstronomyItem) o;
        return Objects.equals(sunriseTime, that.sunriseTime) &&
                Objects.equals(sunsetTime, that.sunsetTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sunriseTime, sunsetTime);
    }
}
