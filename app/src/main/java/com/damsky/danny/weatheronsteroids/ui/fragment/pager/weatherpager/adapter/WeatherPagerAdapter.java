package com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.fragment.daily.DailyWeatherFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.litemap.LiteMapFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.today.TodayWeatherFragment;
import com.damsky.danny.weatheronsteroids.util.Constants;

/**
 * The adapter for {@link WeatherViewPagerFragment}'s ViewPager.
 * ViewPager fragments include {@link TodayWeatherFragment}, and {@link DailyWeatherFragment}
 * and {@link LiteMapFragment} (not present on tablets).
 *
 * @author Danny Damsky.
 */
public final class WeatherPagerAdapter extends FragmentStatePagerAdapter {

    private static final int FRAGMENT_TODAY = 0;
    private static final int FRAGMENT_DAILY = 1;
    private static final int FRAGMENT_MAP = 2;

    private static final int NUM_PAGES_PHONE = 3;
    private static final int NUM_PAGES_TABLET = 2;

    private final Context context;
    private final Bundle bundle;
    private final int numPages;

    public WeatherPagerAdapter(@NonNull WeatherViewPagerFragment fragment) {
        super(fragment.getChildFragmentManager());
        this.context = fragment.getContext();
        this.bundle = fragment.getArguments();
        this.numPages = this.bundle.getBoolean(Constants.EXTRA_IS_LARGE_LAYOUT, false)
                ? NUM_PAGES_TABLET : NUM_PAGES_PHONE;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case FRAGMENT_TODAY:
                return getTodayWeatherFragment();
            case FRAGMENT_DAILY:
                return getDailyWeatherFragment();
            case FRAGMENT_MAP:
                return getLiteMapFragment();
            default:
                throw new IllegalStateException("Unknown item: " + i);
        }
    }

    @NonNull
    private TodayWeatherFragment getTodayWeatherFragment() {
        final TodayWeatherFragment fragment = new TodayWeatherFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    private DailyWeatherFragment getDailyWeatherFragment() {
        final DailyWeatherFragment fragment = new DailyWeatherFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    private LiteMapFragment getLiteMapFragment() {
        return LiteMapFragment.newInstance(bundle);
    }

    @Override
    public int getCount() {
        return numPages;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case FRAGMENT_TODAY:
                return context.getResources().getString(R.string.today);
            case FRAGMENT_DAILY:
                return context.getResources().getString(R.string.forecast);
            case FRAGMENT_MAP:
                return context.getResources().getString(R.string.action_map);
            default:
                throw new IllegalStateException("Unknown item: " + position);
        }
    }
}
