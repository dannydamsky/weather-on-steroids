package com.damsky.danny.weatheronsteroids.util;

import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.util.List;

/**
 * Util class for reverse geo-coding, since that feature isn't provided by the Google
 * Places SDK for Android.
 *
 * @author Danny Damsky
 */
public final class PlaceUtils {

    private static final String JSON_RESULTS = "results";
    private static final String JSON_ID = "id";

    private static final String URL_PREFIX = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=";
    private static final String URL_LATLNG_SEPARATOR = ",";
    private static final String URL_RADIUS = "&radius=500";
    private static final String URL_SUFFIX = "&key=YOUR_KEY_HERE";

    @Nullable
    public static Address getAddress(@NonNull Geocoder geocoder, @NonNull LatLng latLng) {
        try {
            final List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                return addresses.get(0);
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return null;
    }

    @Nullable
    public static String getAddressId(@NonNull Address address) {
        try {
            final String request = getRequest(address);
            final String response = OkHttpUtils.getResponse(request);
            return new JSONObject(response).getJSONArray(JSON_RESULTS).getJSONObject(0).getString(JSON_ID);
        } catch (Exception e) {
            return null;
        }
    }

    @NonNull
    private static String getRequest(@NonNull Address address) {
        return URL_PREFIX +
                address.getLatitude() +
                URL_LATLNG_SEPARATOR +
                address.getLongitude() +
                URL_RADIUS +
                URL_SUFFIX;
    }
}
