package com.damsky.danny.weatheronsteroids.data.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * @author Danny Damsky
 * @see Entity
 */
@Entity(tableName = "locations")
public final class WeatherLocation {

    @NonNull
    @PrimaryKey
    private String id;

    @NonNull
    @ColumnInfo(name = "location_name")
    private String locationName;

    @ColumnInfo(name = "latitude")
    private double latitude;

    @ColumnInfo(name = "longitude")
    private double longitude;

    @ColumnInfo(name = "last_known_response")
    private String lastKnownResponse;

    @ColumnInfo(name = "last_updated")
    private long lastUpdated;

    @ColumnInfo(name = "is_added")
    private boolean isAdded;

    @Ignore
    private WeatherLocation(Builder builder) {
        this.id = builder.id;
        this.locationName = builder.locationName;
        this.latitude = builder.latitude;
        this.longitude = builder.longitude;
        this.lastKnownResponse = builder.lastKnownResponse;
        this.lastUpdated = builder.lastUpdated;
        this.isAdded = builder.isAdded;
    }

    public WeatherLocation() {
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(@NonNull String locationName) {
        this.locationName = locationName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLastKnownResponse() {
        return lastKnownResponse;
    }

    public void setLastKnownResponse(String lastKnownResponse) {
        this.lastKnownResponse = lastKnownResponse;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }

    @Override
    public String toString() {
        return "WeatherLocation{" +
                "id='" + id + '\'' +
                ", locationName='" + locationName + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", lastKnownResponse='" + lastKnownResponse + '\'' +
                ", lastUpdated=" + lastUpdated +
                ", isAdded=" + isAdded +
                '}';
    }

    public static final class Builder {
        private String id;
        private String locationName;
        private double latitude;
        private double longitude;
        private String lastKnownResponse;
        private long lastUpdated;
        private boolean isAdded = true;

        public Builder setId(@NonNull String id) {
            this.id = id;
            return this;
        }

        public Builder setLocationName(@NonNull String locationName) {
            this.locationName = locationName;
            return this;
        }

        public Builder setLatitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder setLongitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder setLastKnownResponse(@NonNull String lastKnownResponse) {
            this.lastKnownResponse = lastKnownResponse;
            return this;
        }

        public Builder setAdded(boolean isAdded) {
            this.isAdded = isAdded;
            return this;
        }

        public WeatherLocation build() {
            this.lastUpdated = System.currentTimeMillis();
            return new WeatherLocation(this);
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "id='" + id + '\'' +
                    ", locationName='" + locationName + '\'' +
                    ", latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", lastKnownResponse='" + lastKnownResponse + '\'' +
                    ", lastUpdated=" + lastUpdated +
                    ", isAdded=" + isAdded +
                    '}';
        }
    }
}
