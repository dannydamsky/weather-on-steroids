package com.damsky.danny.weatheronsteroids.util;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.weather.model.YahooResults;
import com.damsky.danny.weatheronsteroids.util.yahoo.YahooResponseCleaner;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Utilities class that holds a single instance of OkHttpClient that is used throughout the application
 * using the functions provided in this class.
 *
 * @author Danny Damsky
 */
public final class OkHttpUtils {

    private static final OkHttpClient okHttpClient = new OkHttpClient();

    /**
     * @param requestUrl an endpoint URL that points to a query in YAHOO's API
     * @return a response JSON from YAHOO, after all the useless fields have been removed from it.
     * @throws IOException          if the request could not be executed due to cancellation, a connectivity
     *                              problem or timeout. Because networks can fail during an exchange, it is possible that the
     *                              remote server accepted the request before the failure.
     * @throws NullPointerException the function purposefully attempts to invoke a NullPointerException
     *                              in order to make sure that the response provided from YAHOO is indeed valid. (If it's valid,
     *                              then the exception will not be thrown)
     */
    public static String getYahooResponse(@NonNull String requestUrl) throws IOException, NullPointerException {
        final Request request = new Request.Builder().url(requestUrl).build();
        final Response response = okHttpClient.newCall(request).execute();
        final String json = YahooResponseCleaner.clean(response.body().string());

        // Attempt to invoke NullPointerException
        // If it gets invoked then the JSON is incorrect.
        final YahooResults results = GsonUtils.fromJson(json, YahooResults.class);
        results.getChannel().get(0).getItem().getCondition().getConditionCode();

        return json;
    }

    /**
     * Invokes the request immediately, and blocks until the response can be processed or is in
     * error.
     *
     * @param requestUrl the url to download data from.
     * @return the response from the website.
     * @throws IOException if the request could not be executed due to cancellation, a connectivity
     *                     problem or timeout. Because networks can fail during an exchange, it is possible that the
     *                     remote server accepted the request before the failure.
     */
    public static String getResponse(@NonNull String requestUrl) throws IOException {
        final Request request = new Request.Builder().url(requestUrl).build();
        final Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }
}
