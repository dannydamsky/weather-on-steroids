package com.damsky.danny.weatheronsteroids.ui.fragment.locations.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The adapter for the RecyclerView of {@link com.damsky.danny.weatheronsteroids.ui.fragment.locations.LocationsWeatherFragment}.
 * This adapter shows locations and their current weather in a list.
 *
 * @author Danny Damsky
 */
public final class LocationsRecyclerAdapter extends ListAdapter<OptimizedLocationItem,
        LocationsRecyclerAdapter.ViewHolder> {

    private Context context;
    private Listener listener;

    public LocationsRecyclerAdapter() {
        super(OptimizedLocationItem.DIFF_CALLBACK);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        listener = (Listener) context;
        final View itemView = LayoutInflater.from(context)
                .inflate(R.layout.row_locations, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final OptimizedLocationItem location = getItem(i);
        viewHolder.locationTextView.setText(location.getLocation().getLocationName());
        viewHolder.myLocationImage.setVisibility(View.GONE);
        loadTempIntoTextView(viewHolder, location);
        loadWeatherImageIntoImageView(viewHolder, location);
        setRowLayoutClickListeners(viewHolder, location);
    }

    private void loadTempIntoTextView(@NonNull ViewHolder viewHolder, @NonNull OptimizedLocationItem item) {
        final int currentTemp = item.getCondition().getCurrentTemp();
        final String tempUnit = item.getTempUnit();
        final String tempString = context.getResources().getString(R.string.degree, currentTemp) + tempUnit;
        viewHolder.tempTextView.setText(tempString);
    }

    private void loadWeatherImageIntoImageView(@NonNull ViewHolder viewHolder, @NonNull OptimizedLocationItem item) {
        viewHolder.weatherImage.setImageResource(item.getCondition().getConditionImageId());
    }

    private void setRowLayoutClickListeners(@NonNull ViewHolder viewHolder, @NonNull OptimizedLocationItem location) {
        viewHolder.rowLayout.setOnClickListener(view -> listener.onItemClicked(location));
        viewHolder.rowLayout.setOnLongClickListener(view -> {
            listener.onItemLongClicked(location);
            return false;
        });
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rowLayout)
        ConstraintLayout rowLayout;
        @BindView(R.id.weatherImage)
        ImageView weatherImage;
        @BindView(R.id.locationTextView)
        TextView locationTextView;
        @BindView(R.id.myLocationImage)
        ImageView myLocationImage;
        @BindView(R.id.tempTextView)
        TextView tempTextView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface Listener {
        void onItemClicked(@NonNull OptimizedLocationItem location);

        void onItemLongClicked(@NonNull OptimizedLocationItem location);
    }
}
