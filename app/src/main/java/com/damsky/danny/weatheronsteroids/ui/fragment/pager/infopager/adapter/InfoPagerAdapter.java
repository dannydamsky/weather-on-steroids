package com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.fragment.about.AboutFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.prefs.PreferencesFragment;

/**
 * The adapter for {@link InfoPagerFragment}'s ViewPager.
 * ViewPager fragments include {@link AboutFragment} and {@link PreferencesFragment}.
 *
 * @author Danny Damsky.
 */
public final class InfoPagerAdapter extends FragmentStatePagerAdapter {

    private static final int FRAGMENT_ABOUT = 0;
    private static final int FRAGMENT_PREFERENCES = 1;
    private static final int NUM_PAGES = 2;

    private final Context context;

    public InfoPagerAdapter(@NonNull InfoPagerFragment fragment) {
        super(fragment.getChildFragmentManager());
        this.context = fragment.getContext();
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case FRAGMENT_ABOUT:
                return new AboutFragment();
            case FRAGMENT_PREFERENCES:
                return new PreferencesFragment();
            default:
                throw new IllegalStateException("Unknown item: " + i);
        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case FRAGMENT_ABOUT:
                return context.getString(R.string.about);
            case FRAGMENT_PREFERENCES:
                return context.getString(R.string.action_settings);
            default:
                throw new IllegalStateException("Unknown item: " + position);
        }
    }
}
