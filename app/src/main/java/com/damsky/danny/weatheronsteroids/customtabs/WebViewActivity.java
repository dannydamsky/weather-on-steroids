package com.damsky.danny.weatheronsteroids.customtabs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.widget.MaterialSnackbar;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An activity that mimics the look and feel of Chrome Custom Tabs.
 * This activity is a fallback to devices that do not have Chrome Custom Tabs installed.
 *
 * @author Danny Damsky
 */
public final class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.mainCoordinatorLayout)
    CoordinatorLayout mainCoordinatorLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webView)
    WebView webView;

    private MenuItem goForwardItem;

    private String currentUrl;

    // Download parameters
    private String url;
    private String userAgent;
    private String contentDisposition;
    private String mimeType;

    /**
     * A fully functional web browser needs to support JavaScript.
     */
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        currentUrl = getIntent().getStringExtra(Constants.EXTRA_URL);
        getSupportActionBar().setTitle(currentUrl);

        webView.loadUrl(currentUrl);

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                goForwardItem.setEnabled(webView.canGoForward());
                currentUrl = url;
                getSupportActionBar().setTitle(view.getTitle());
                getSupportActionBar().setSubtitle(url);
            }
        });

        webView.setDownloadListener((url, userAgent, contentDisposition, mimeType, l) -> {
            this.url = url;
            this.userAgent = userAgent;
            this.contentDisposition = contentDisposition;
            this.mimeType = mimeType;
            validateStoragePermissions();
        });
    }

    private void validateStoragePermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE_WRITE_STORAGE_PERMISSION);
        } else {
            download();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            download();
        } else {
            MaterialSnackbar.make(mainCoordinatorLayout, R.string.error_permissions_not_granted_download, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_web, menu);
        goForwardItem = menu.findItem(R.id.forward_item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.forward_item:
                onForwardPressed();
                break;
            case R.id.download_item:
                this.url = currentUrl;
                download();
                break;
            case R.id.refresh_item:
                webView.reload();
                break;
            case R.id.share_item:
                IntentUtils.startShareIntent(this, currentUrl);
                break;
            case R.id.browser_item:
                IntentUtils.startWebsite(this, Uri.parse(currentUrl));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onForwardPressed() {
        if (webView.canGoForward()) {
            webView.goForward();
        }
        goForwardItem.setEnabled(webView.canGoForward());
    }

    private void download() {
        final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setMimeType(mimeType);
        request.addRequestHeader("cookie", CookieManager.getInstance().getCookie(url));
        request.addRequestHeader("User-Agent", userAgent);
        request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        try {
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                    URLUtil.guessFileName(url, contentDisposition, mimeType));
            final DownloadManager dm = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            dm.enqueue(request);
            MaterialSnackbar.make(mainCoordinatorLayout, R.string.file_download, Snackbar.LENGTH_SHORT).show();
        } catch (IllegalStateException e) {
            MaterialSnackbar.make(mainCoordinatorLayout, R.string.error_file_download_create_dir, Snackbar.LENGTH_LONG).show();
        } catch (NullPointerException e) {
            MaterialSnackbar.make(mainCoordinatorLayout, R.string.error_no_download_manager, Snackbar.LENGTH_LONG).show();
        }
    }
}
