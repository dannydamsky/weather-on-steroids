package com.damsky.danny.weatheronsteroids.data.weather.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author Danny Damsky
 * @see YahooResults
 */
public final class YahooCondition {

    @SerializedName("code")
    private final int conditionCode;
    @SerializedName("temp")
    private final int currentTemp;

    public YahooCondition(int conditionCode, int currentTemp) {
        this.conditionCode = conditionCode;
        this.currentTemp = currentTemp;
    }

    public int getConditionCode() {
        return conditionCode;
    }

    public int getCurrentTemp() {
        return currentTemp;
    }

    @Override
    public String toString() {
        return "YahooCondition{" +
                "conditionCode=" + conditionCode +
                ", currentTemp=" + currentTemp +
                '}';
    }
}
