package com.damsky.danny.weatheronsteroids.work;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationRepository;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.firestore.FirestoreHelper;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.damsky.danny.weatheronsteroids.util.OkHttpUtils;
import com.damsky.danny.weatheronsteroids.util.reactive.Optional;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.Places;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkStatus;
import androidx.work.Worker;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Worker class that handles database operations including: insert, update single, update all, and
 * sync with firebase.
 * This class connects to YAHOO Weather API to provide the latest weather info and to Google Places API
 * to provide photographs for each location.
 *
 * @author Danny Damsky
 * @see WorkManager
 * @see Worker
 */
public final class DbOpsWorker extends Worker {

    private static final String LOG_TAG = "WORK_STATUS";

    @NonNull
    private static OneTimeWorkRequest.Builder getDefaultBuilder() {
        return new OneTimeWorkRequest.Builder(DbOpsWorker.class)
                .setConstraints(getInternetConstraint());
    }

    @NonNull
    private static Constraints getInternetConstraint() {
        return new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
    }

    private static void setStatusListener(@NonNull AppCompatActivity activity, @NonNull UUID workRequestId) {
        final LiveData<WorkStatus> liveData = WorkManager.getInstance().getStatusById(workRequestId);
        liveData.observe(activity, workStatus -> {
            if (workStatus != null) {
                switch (workStatus.getState()) {
                    case FAILED:
                        FancyToast.makeText(activity.getApplicationContext(),
                                activity.getString(R.string.error_location_not_found),
                                FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
                        Log.d(LOG_TAG, "setStatusListener: FAILED");
                        break;
                    case BLOCKED:
                        Log.d(LOG_TAG, "setStatusListener: BLOCKED");
                        break;
                    case RUNNING:
                        Log.d(LOG_TAG, "setStatusListener: RUNNING");
                        break;
                    case ENQUEUED:
                        Log.d(LOG_TAG, "setStatusListener: ENQUEUED");
                        break;
                    case CANCELLED:
                        Log.d(LOG_TAG, "setStatusListener: CANCELLED");
                        break;
                    case SUCCEEDED:
                        Log.d(LOG_TAG, "setStatusListener: SUCCEEDED");
                        break;
                }
            }
        });
    }

    /**
     * Enqueues the worker to make sure that the local database and Firebase Firestore
     * are synchronized.
     *
     * @param activity The activity from where the function is being called.
     */
    public static void syncWithFirebase(@NonNull AppCompatActivity activity) {
        final OneTimeWorkRequest request = getDefaultBuilder()
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_SYNC_WITH_FIREBASE)
                        .buildData()).build();
        WorkManager.getInstance().enqueue(request);
        setStatusListener(activity, request.getId());
    }

    /**
     * Enqueues a worker to add a new location to the database and to
     * Firebase Firestore. (if the user is logged in)
     * Using this function ensures that before the place is added to the database, the worker
     * will connect to YAHOO API to get the latest weather results for the location.
     * Also, after the location has been added to the database, the worker will fetch
     * photos from Google Places API and also add them to the local database.
     *
     * @param activity The activity from where the function is being called.
     * @param place    The place to add to the database.
     */
    public static void insertNewPlace(@NonNull AppCompatActivity activity, @NonNull Place place) {
        final OneTimeWorkRequest request = getDefaultBuilder()
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_INSERT)
                        .setPlace(place, true)
                        .buildData()).build();
        WorkManager.getInstance().enqueue(request);
        setStatusListener(activity, request.getId());
    }

    /**
     * Enqueues a worker to add a new location to the database and to
     * Firebase Firestore. (if the user is logged in)
     * Using this function ensures that before the place is added to the database, the worker
     * will connect to the YAHOO API to get the latest weather results for the location.
     * Also, after the location has been added to the database, the worker will fetch
     * photos from Google Places API and also add them to the local database.
     *
     * @param weatherLocation The location to add to the database.
     * @return The work ID so that the activity from where the function was called can decide to
     * listen for status changes in the worker.
     */
    @NonNull
    public static UUID insertNewPlace(@NonNull WeatherLocation weatherLocation) {
        final OneTimeWorkRequest request = getDefaultBuilder()
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_INSERT)
                        .setWeatherLocation(weatherLocation)
                        .buildData()).build();
        WorkManager.getInstance().enqueue(request);
        return request.getId();
    }

    /**
     * Enqueues a worker to update a location in the database and in
     * Firebase Firestore. (if the user is logged in)
     * Using this function ensures that before the location is updated in the database, the worker
     * will connect to the YAHOO API to get the latest weather results for the location.
     * Also, after the location has been updated in the database, the worker will fetch
     * photos from Google Places API and also add them to the local database. (if they don't already exist)
     *
     * @param activity The activity from where the function is being called.
     * @param location The location to update in the database.
     */
    public static void updateSinglePlace(@NonNull AppCompatActivity activity, @NonNull OptimizedWeatherLocation location) {
        final OneTimeWorkRequest request = getDefaultBuilder()
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_UPDATE_SINGLE)
                        .setWeatherLocation(location)
                        .buildData()).build();
        WorkManager.getInstance().enqueue(request);
        setStatusListener(activity, request.getId());
    }

    /**
     * Enqueues a worker to update all locations in the database and in
     * Firebase Firestore. (if the user is logged in)
     * Using this function ensures that before the locations are updated in the database, the worker
     * will connect to the YAHOO API to get the latest weather results for the locations.
     * Also, after the locations have been updated in the database, the worker will fetch
     * photos from Google Places API and also add them to the local database. (if they don't already exist)
     *
     * @param activity The activity from where the function is being called.
     */
    public static void updateAllPlaces(@NonNull AppCompatActivity activity) {
        final OneTimeWorkRequest request = getDefaultBuilder()
                .setInputData(new DbOpsData.Builder()
                        .setRequestCode(DbOpsData.REQUEST_CODE_UPDATE_ALL)
                        .buildData()).build();
        WorkManager.getInstance().enqueue(request);
        setStatusListener(activity, request.getId());
    }

    private WeatherLocationRepository repository;
    private DbOpsData intent;
    private WeatherLocation location;
    private GeoDataClient geoDataClient;

    @NonNull
    @Override
    public Result doWork() {
        repository = WeatherLocationRepository.getInstance(getApplicationContext());
        intent = new DbOpsData(getInputData());
        geoDataClient = Places.getGeoDataClient(getApplicationContext());
        return getResult();
    }

    private Worker.Result getResult() {
        try {
            handleRequestCode();
            return Result.SUCCESS;
        } catch (Exception e) {
            Crashlytics.logException(e);
            return Result.FAILURE;
        }
    }

    private void handleRequestCode() throws IOException {
        switch (intent.getRequestCode()) {
            case DbOpsData.REQUEST_CODE_INSERT:
                insertNewLocation();
                break;
            case DbOpsData.REQUEST_CODE_UPDATE_SINGLE:
                updateSingleLocation();
                break;
            case DbOpsData.REQUEST_CODE_UPDATE_ALL:
                updateAllLocations();
                break;
            case DbOpsData.REQUEST_CODE_SYNC_WITH_FIREBASE:
                syncWithFirebase();
                break;
        }
    }

    private void insertNewLocation() throws IOException, NullPointerException {
        final String json = OkHttpUtils.getYahooResponse(intent.getRequestUrl());
        this.location = intent.buildWeatherLocation(json);
        repository.insert(location);
        downloadPhotos(location.getId());
    }

    private void updateSingleLocation() throws IOException {
        this.location = repository.getByIdBlocking(intent.getRequestId());
        updateProperties();
        repository.update(location);
        downloadPhotos(location.getId());
    }

    private void updateAllLocations() throws IOException {
        final List<WeatherLocation> locationList = repository.getAllBlocking();
        final String[] places = iterateOverLocations(locationList);
        repository.updateAll(locationList);
        downloadPhotos(places);
    }

    private String[] iterateOverLocations(@NonNull List<WeatherLocation> locationList) throws IOException {
        final String[] places = new String[locationList.size()];
        final int size = locationList.size();
        for (int i = 0; i < size; i++) {
            this.location = locationList.get(i);
            this.intent.setRequestUrl(location);
            places[i] = this.location.getId();
            updateProperties();
        }
        return places;
    }

    private void updateProperties() throws IOException, NullPointerException {
        final String json = OkHttpUtils.getYahooResponse(intent.getRequestUrl());
        this.location.setLastKnownResponse(json);
        this.location.setLastUpdated(System.currentTimeMillis());
    }

    private void downloadPhotos(@NonNull String... places) {
        for (String placeId : places) {
            geoDataClient.getPlacePhotos(placeId).addOnSuccessListener(placePhotoMetadataResponse ->
                    iterateOverMetadataResponse(placeId, placePhotoMetadataResponse))
                    .addOnFailureListener(Crashlytics::logException);
        }
    }

    private void iterateOverMetadataResponse(@NonNull String placeId, @NonNull PlacePhotoMetadataResponse placePhotoMetadataResponse) {
        final PlacePhotoMetadataBuffer buffer = placePhotoMetadataResponse.getPhotoMetadata();
        for (PlacePhotoMetadata photo : buffer) {
            getNextBitmap(placeId, photo);
        }
        buffer.release();
    }

    private void getNextBitmap(@NonNull String placeId, @NonNull PlacePhotoMetadata photo) {
        geoDataClient.getPhoto(photo).addOnSuccessListener(placePhotoResponse -> {
            final Bitmap bitmap = placePhotoResponse.getBitmap();
            if (bitmap != null) {
                repository.insert(new LocationPhoto.Builder()
                        .setParentId(placeId)
                        .setBitmap(bitmap).build());
            }
        }).addOnFailureListener(Crashlytics::logException);
    }

    private void syncWithFirebase() {
        FirestoreHelper.getAllFromFirebase(locations -> {
            insertToAndFromFirebase(locations);
            for (WeatherLocation location : locations) {
                geoDataClient.getPlacePhotos(location.getId()).addOnSuccessListener(placePhotoMetadataResponse ->
                        iterateOverMetadataResponse(location.getId(), placePhotoMetadataResponse))
                        .addOnFailureListener(Crashlytics::logException);
            }
        });
    }

    /**
     * The result in the .defer lambda will definitely be an {@link Optional} class.
     */
    @SuppressLint("CheckResult")
    private void insertToAndFromFirebase(@NonNull WeatherLocation[] locations) {
        Observable.defer((Callable<ObservableSource<?>>) () -> {
            repository.insertAllBlocking(locations);
            return Observable.just(new Optional<>(null));
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> repository.insertOrUpdateAllInFirebase());
    }

}
