package com.damsky.danny.weatheronsteroids.data.firestore;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.data.auth.AuthHelper;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.util.reactive.Optional;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * A helper class for Firebase Firestore work.
 *
 * @author Danny Damsky
 */
public final class FirestoreHelper {

    private FirestoreHelper() {
    }

    @Nullable
    public static CollectionReference getUserCollection() {
        if (AuthHelper.isLoggedIn()) {
            return FirebaseFirestore.getInstance()
                    .collection(AuthHelper.getUserId());
        }
        return null;
    }

    @NonNull
    public static DocumentReference getLocationDocument(@NonNull CollectionReference userCollection, @NonNull WeatherLocation location) {
        return userCollection.document(location.getId());
    }

    public static void getAllFromFirebase(@NonNull FirebaseCallback<WeatherLocation[]> callback) {
        final CollectionReference userCollection = FirestoreHelper.getUserCollection();
        if (userCollection != null) {
            userCollection.get().addOnSuccessListener(queryDocumentSnapshots ->
                    asyncGetAllFromFirebase(callback, queryDocumentSnapshots))
                    .addOnFailureListener(Crashlytics::logException);
        }
    }

    /**
     * The result from the .flatMap lambda will definitely be an {@link Optional} of
     * {@link WeatherLocation} array.
     */
    @SuppressLint("CheckResult")
    private static void asyncGetAllFromFirebase(@NonNull FirebaseCallback<WeatherLocation[]> callback,
                                                @NonNull QuerySnapshot queryDocumentSnapshots) {
        Observable.just(queryDocumentSnapshots)
                .flatMap(queryDocumentSnapshots1 ->
                        Observable.just(new Optional<>(synchronousGetAllFromFirebase(queryDocumentSnapshots1))))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(optional -> {
                    if (!optional.isEmpty()) {
                        callback.onSuccess(optional.get());
                    }
                });
    }

    @Nullable
    private static WeatherLocation[] synchronousGetAllFromFirebase(@NonNull QuerySnapshot queryDocumentSnapshots) {
        final int size = queryDocumentSnapshots.size();
        if (size > 0) {
            final WeatherLocation[] locations = new WeatherLocation[size];
            final List<DocumentSnapshot> snapshots = queryDocumentSnapshots.getDocuments();
            for (int i = 0; i < size; i++) {
                locations[i] = snapshots.get(i).toObject(WeatherLocation.class);
            }
            return locations;
        }
        return null;
    }

    public interface FirebaseCallback<T> {
        void onSuccess(@NonNull T obj);
    }
}
