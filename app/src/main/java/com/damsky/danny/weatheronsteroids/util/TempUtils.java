package com.damsky.danny.weatheronsteroids.util;

import android.support.annotation.IntRange;


/**
 * Temperature Utilities class for calculating things such as RealFeel and
 * converting between Celsius and Fahrenheit.
 *
 * @author Danny Damsky
 * @see <a href="http://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml">
 * The Heat Index Equation
 * </a>
 */
public final class TempUtils {

    /**
     * Calculates the approximate temperature of how it feels like outside.
     * Absolute error: 1 degree.
     *
     * @param temp             The temperature in Celsius
     * @param relativeHumidity The relative humidity in percent
     * @return The heat index temperature in Celsius
     */
    public static int getRealFeelTempMetric(int temp,
                                            @IntRange(from = 0, to = 100) int relativeHumidity) {
        return (int) convertToCelsius(
                getRealFeelTempImperial(convertToFahrenheit(temp), relativeHumidity)
        );
    }

    /**
     * Calculates the approximate temperature of how it feels like outside.
     * Absolute error: 1 degree.
     *
     * @param temp             The temperature in Fahrenheit
     * @param relativeHumidity The relative humidity in percent
     * @return The heat index temperature in Fahrenheit
     */
    public static int getRealFeelTempImperial(int temp,
                                              @IntRange(from = 0, to = 100) int relativeHumidity) {
        return (int) getRealFeelTempImperial((double) temp, relativeHumidity);
    }

    private static double getRealFeelTempImperial(final double T,
                                                  @IntRange(from = 0, to = 100) int relativeHumidity) {
        final double RH = (double) relativeHumidity;
        if (T >= 80) {
            final double HI = getHeatIndexByRothfusz(T, RH);
            double ADJUSTMENT = 0;
            if (T > 80 && T < 112 && RH < 0.13) {
                ADJUSTMENT = ((13 - RH) / 4) * Math.sqrt((17 - Math.abs(T - 95)) / 17);
            } else if (T > 80 && T < 87 && RH > 0.85) {
                ADJUSTMENT = ((RH - 85) / 10) * ((87 - T) / 5);
            }
            return HI - ADJUSTMENT;
        } else {
            return (T + calculateBySteadman(T, RH)) / 2;
        }
    }

    /**
     * Calculates the heat index for temperatures above 80F
     *
     * @param T  The temperature in Fahrenheit
     * @param RH The relative humidity in percent
     * @return The heat index calculated by Rothfusz's formula
     */
    private static double getHeatIndexByRothfusz(double T, double RH) {
        return -42.379 + (2.04901523 * T) + (10.14333127 * RH)
                - (0.22475541 * T * RH) - (0.00683783 * T * T)
                - (0.05481717 * RH * RH) + (0.00122874 * T * T * RH)
                + (0.00085282 * T * RH * RH) - (0.00000199 * T * T * RH * RH);
    }

    /**
     * Calculates the heat index for temperatures below 80F
     *
     * @param T  The temperature in Fahrenheit
     * @param RH The relative humidity in percent
     * @return The heat index calculated by Steadman's formula
     */
    private static double calculateBySteadman(double T, double RH) {
        return 0.5 * (T + 61.0 + ((T - 68.0) * 1.2) + (RH * 0.094));
    }

    public static int convertToCelsius(int fahrenheit) {
        return (int) convertToCelsius((double) fahrenheit);
    }

    private static double convertToCelsius(double f) {
        return (f - 32) / 1.8;
    }

    private static double convertToFahrenheit(int celsius) {
        final double c = (double) celsius;
        return (c * 1.8) + 32;
    }

    private TempUtils() {
    }
}
