package com.damsky.danny.weatheronsteroids.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;

import com.crashlytics.android.Crashlytics;

import java.io.ByteArrayOutputStream;

/**
 * Utils class for working with images.
 *
 * @author Danny Damsky
 */
public final class ImageUtils {

    @NonNull
    public static String bitmapToString(@NonNull Bitmap bitmap) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        final byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    @Nullable
    public static Bitmap stringToBitmap(@NonNull String encodedString) {
        Bitmap bitmap = null;
        try {
            final byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        return bitmap;
    }

}
