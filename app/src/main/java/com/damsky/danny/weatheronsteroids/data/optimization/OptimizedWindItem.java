package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.weather.model.YahooWind;
import com.damsky.danny.weatheronsteroids.util.TempUtils;

import java.util.Objects;

/**
 * This class is contained inside {@link OptimizedLocationItem}.
 * The class contains information about the location's wind properties.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see YahooWind
 * @see TempUtils
 */
public final class OptimizedWindItem {

    private final int chillTemp;
    private final int directionDegrees;
    private final double speed;

    OptimizedWindItem(@NonNull YahooWind wind, boolean isMetric) {
        if (isMetric) {
            this.chillTemp = TempUtils.convertToCelsius(wind.getChill());
        } else {
            this.chillTemp = wind.getChill();
        }
        this.directionDegrees = wind.getDirection();
        this.speed = wind.getSpeed();
    }

    public int getChillTemp() {
        return chillTemp;
    }

    public int getDirectionDegrees() {
        return directionDegrees;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "OptimizedWindItem{" +
                "chillTemp=" + chillTemp +
                ", directionDegrees=" + directionDegrees +
                ", speed=" + speed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedWindItem)) return false;
        final OptimizedWindItem that = (OptimizedWindItem) o;
        return chillTemp == that.chillTemp &&
                directionDegrees == that.directionDegrees &&
                Double.compare(that.speed, speed) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(chillTemp, directionDegrees, speed);
    }
}
