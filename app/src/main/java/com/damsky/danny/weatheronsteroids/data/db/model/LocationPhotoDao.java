package com.damsky.danny.weatheronsteroids.data.db.model;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

/**
 * @author Danny Damsky
 * @see LocationPhoto
 * @see Dao
 */
@Dao
public interface LocationPhotoDao {

    @Query("SELECT * FROM photos where parent_id = :placeId")
    DataSource.Factory<Integer, LocationPhoto> getLocationPhotosDataSource(String placeId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(LocationPhoto locationPhoto);
}
