package com.damsky.danny.weatheronsteroids.ui.fragment.mainmap;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.maps.MapClusterItem;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.ui.widget.MaterialSnackbar;
import com.damsky.danny.weatheronsteroids.util.PlaceUtils;
import com.damsky.danny.weatheronsteroids.util.reactive.Optional;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.clustering.ClusterManager;

import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * This is the main (full-featured) map fragment, it is contained inside
 * {@link com.damsky.danny.weatheronsteroids.ui.fragment.mapholder.MapHolderFragment}.
 * <br /><br />
 * When using this class make sure to implement {@link Listener}.
 * <br /> <br />
 * The {@link SuppressLint} is used here because the application cannot run without location access
 * permissions (which are the required permission for this fragment's map object).
 *
 * @author Danny Damsky
 */
@SuppressLint("MissingPermission")
public final class MapFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnPoiClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMapClickListener {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.mainmap.MapFragment";

    private Disposable mapClickTask;

    /**
     * Shows the fragment inside the parent layout.
     *
     * @param fragmentManager the fragment manager to hold the fragment.
     * @param parentLayoutId  the ID of the layout that the fragment will be contained in.
     */
    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            final MapFragment fragment = newInstance();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(parentLayoutId, fragment, TAG);
            transaction.commit();
        }
    }

    /**
     * Overrides its parent class' newInstance() function to provide custom map options when
     * initializing this class.
     *
     * @return a {@link MapFragment} object.
     * @see SupportMapFragment#newInstance(GoogleMapOptions)
     */
    public static MapFragment newInstance() {
        final MapFragment mapFragment = new MapFragment();
        final Bundle args = new Bundle();
        args.putParcelable("MapOptions", getOptions());
        mapFragment.setArguments(args);
        return mapFragment;
    }

    private static GoogleMapOptions getOptions() {
        return new GoogleMapOptions()
                .mapToolbarEnabled(false)
                .zoomGesturesEnabled(true)
                .zoomControlsEnabled(false)
                .tiltGesturesEnabled(true)
                .scrollGesturesEnabled(true)
                .rotateGesturesEnabled(true)
                .compassEnabled(true);
    }

    private Context context;
    private Listener listener;
    private PlaceDetectionClient placeDetectionClient;
    private GoogleMap map;
    private Geocoder geocoder;

    private WeatherLocationViewModel weatherLocationViewModel;
    private List<MapClusterItem> locations;
    private ClusterManager<MapClusterItem> clusterManager;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.listener = (Listener) getParentFragment();
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
        placeDetectionClient = Places.getPlaceDetectionClient(context);
        geocoder = new Geocoder(context, Locale.getDefault());
    }

    @Override
    public void onStart() {
        super.onStart();
        getMapAsync(this);
        setLocationMarkersObserver();
    }

    private void setLocationMarkersObserver() {
        weatherLocationViewModel.getLocationMarkers().observe(this, mapClusterItems -> {
            this.locations = mapClusterItems;
            if (map != null) {
                loadMarkers();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        setMapProperties();
        zoomToCurrentLocationAsap();
        configClusterManager();
        if (locations != null) {
            loadMarkers();
        }
    }

    private void setMapProperties() {
        map.setBuildingsEnabled(true);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (PreferencesHelper.getInstance().isNightModeEnabled()) {
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.maps_night_mode));
        }
        map.setOnPoiClickListener(this);
        map.setOnInfoWindowClickListener(this);
        map.setOnMapClickListener(this);
    }

    public void zoomToCurrentLocation() {
        final Task<PlaceLikelihoodBufferResponse> taskToGetCurrentLocation = placeDetectionClient.getCurrentPlace(null);
        taskToGetCurrentLocation.addOnCompleteListener(task -> {
            Place currentLocation = tryGettingLocation(task);
            if (currentLocation != null) {
                zoomToPlace(currentLocation.getLatLng());
            }
        });
    }

    private void zoomToCurrentLocationAsap() {
        final Task<PlaceLikelihoodBufferResponse> taskToGetCurrentLocation = placeDetectionClient.getCurrentPlace(null);
        taskToGetCurrentLocation.addOnCompleteListener(task -> {
            Place currentLocation = tryGettingLocation(task);
            if (currentLocation != null) {
                zoomToPlaceImmediately(currentLocation.getLatLng());
            }
        });
    }

    private Place tryGettingLocation(Task<PlaceLikelihoodBufferResponse> task) {
        try {
            return getUserMostLikelyLocation(task);
        } catch (RuntimeExecutionException e) {
            if (getView() != null) {
                MaterialSnackbar.make(getView(),
                        R.string.error_detect_location, Snackbar.LENGTH_LONG).show();
            }
        }
        return null;
    }

    private Place getUserMostLikelyLocation(@NonNull Task<PlaceLikelihoodBufferResponse> task) throws RuntimeExecutionException {
        final PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
        final PlaceLikelihood mostLikelyPlace = getMostLikelyPlace(likelyPlaces).freeze();
        likelyPlaces.release();
        return mostLikelyPlace.getPlace();
    }

    private PlaceLikelihood getMostLikelyPlace(@NonNull PlaceLikelihoodBufferResponse likelyPlaces) {
        final int count = likelyPlaces.getCount();
        PlaceLikelihood mostLikelyPlace = likelyPlaces.get(0);
        for (int i = 1; i < count; i++) {
            PlaceLikelihood nextPlace = likelyPlaces.get(i);
            if (nextPlace.getLikelihood() > mostLikelyPlace.getLikelihood()) {
                mostLikelyPlace = nextPlace;
            }
        }
        return mostLikelyPlace;
    }

    private void zoomToPlaceImmediately(@NonNull LatLng coordinates) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15.0f));
    }

    public void zoomToPlace(@NonNull LatLng coordinates) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15.0f));
    }

    public void zoomTo(float zoom) {
        map.moveCamera(CameraUpdateFactory.zoomTo(zoom));
    }

    private void configClusterManager() {
        this.clusterManager = new ClusterManager<>(context, map);
        map.setOnCameraIdleListener(clusterManager);
        map.setOnMarkerClickListener(clusterManager);
    }

    private void loadMarkers() {
        clusterManager.clearItems();
        clusterManager.cluster();
        clusterManager.addItems(locations);
    }

    @Override
    public void onPoiClick(PointOfInterest pointOfInterest) {
        listener.onMapClicked(pointOfInterest.placeId, pointOfInterest.name, pointOfInterest.latLng, false);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        listener.onMapClicked(marker.getId(), marker.getTitle(), marker.getPosition(), false);
    }

    /**
     * The result from the .flatMap function will definitely be an {@link Optional}
     * of {@link Pair} of {@link Address} and {@link String}.
     */
    @SuppressLint("CheckResult")
    @Override
    public void onMapClick(LatLng latLng) {
        mapClickTask = Observable.just(latLng)
                .flatMap(mapLatLngToPlace())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumePlaceResult(latLng));
    }

    private Function<LatLng, ObservableSource<Optional<Pair<Address, String>>>> mapLatLngToPlace() {
        return latLng -> {
            final Address address = PlaceUtils.getAddress(geocoder, latLng);
            if (address != null) {
                final String addressId = PlaceUtils.getAddressId(address);
                if (addressId != null) {
                    return Observable.just(new Optional<>(new Pair<>(address, addressId)));
                }
            }
            return Observable.just(new Optional<>(null));
        };
    }

    private Consumer<Optional<Pair<Address, String>>> consumePlaceResult(LatLng latLng) {
        return pairOptional -> {
            if (pairOptional.isEmpty()) {
                listener.onMapClicked(null, null, null, true);
            } else {
                Pair<Address, String> pair = pairOptional.get();
                listener.onMapClicked(pair.second, pair.first.getLocality(), latLng, true);
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mapClickTask != null && !mapClickTask.isDisposed()) {
            mapClickTask.dispose();
        }
    }

    public interface Listener {
        void onMapClicked(@Nullable String locationId, @Nullable String locationName, @Nullable LatLng latLng, boolean shouldDismiss);
    }
}
