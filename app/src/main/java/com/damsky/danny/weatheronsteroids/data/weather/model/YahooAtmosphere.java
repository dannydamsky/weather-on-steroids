package com.damsky.danny.weatheronsteroids.data.weather.model;

/**
 * This class is contained inside of {@link YahooChannel}.
 * It contains weather information such as humidity, pressure and visibility.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 */
public final class YahooAtmosphere {

    private final int humidity;
    private final float pressure;
    private final float visibility;

    public YahooAtmosphere(int humidity, float pressure, float visibility) {
        this.humidity = humidity;
        this.pressure = pressure;
        this.visibility = visibility;
    }

    public int getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public float getVisibility() {
        return visibility;
    }

    @Override
    public String toString() {
        return "YahooAtmosphere{" +
                "humidity=" + humidity +
                ", pressure=" + pressure +
                ", visibility=" + visibility +
                '}';
    }
}
