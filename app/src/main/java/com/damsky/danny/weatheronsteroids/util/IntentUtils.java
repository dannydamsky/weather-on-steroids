package com.damsky.danny.weatheronsteroids.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.AlarmClock;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.customtabs.CustomTabsHelper;
import com.damsky.danny.weatheronsteroids.customtabs.WebViewFallback;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.widget.MaterialSnackbar;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.shashank.sony.fancytoastlib.FancyToast;


/**
 * Utils class for starting explicit and implicit intents.
 *
 * @author Danny Damsky
 */
public final class IntentUtils {

    /**
     * Starts a website in a browser or another application that supports the URI provided.
     *
     * @param context the context from where the function is being called.
     * @param uri     the URI of the website
     */
    public static void startWebsite(@NonNull Context context, @NonNull Uri uri) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(uri);
        context.startActivity(intent);
    }

    /**
     * This is the preferred function for starting a website.
     * The function starts the website in Custom Tabs if the device supports them,
     * otherwise it falls back and opens the WebViewActivity.
     *
     * @param context the context from where the function is being called.
     * @param uri     the URI of the website
     * @see <a href="https://developer.chrome.com/multidevice/android/customtabs">Chrome Custom Tabs</a>
     * @see com.damsky.danny.weatheronsteroids.customtabs.WebViewActivity
     * @see CustomTabsIntent
     * @see CustomTabsHelper
     * @see WebViewFallback
     */
    public static void startCustomTabs(@NonNull Context context, @NonNull Uri uri) {
        final CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .addDefaultShareMenuItem()
                .setToolbarColor(context.getResources().getColor(R.color.colorPrimary))
                .setShowTitle(true)
                .setStartAnimations(context, R.anim.slide_in_right_short, R.anim.slide_out_left_short)
                .setExitAnimations(context, R.anim.slide_in_left_short, R.anim.slide_out_right_short)
                .setCloseButtonIcon(getBitmapFromVectorDrawable(context,
                        R.drawable.ic_arrow_back_primary_black_24dp))
                .build();
        CustomTabsHelper.addKeepAliveExtra(context, customTabsIntent.intent);
        CustomTabsHelper.openCustomTab(context, customTabsIntent, uri, new WebViewFallback());
    }

    private static Bitmap getBitmapFromVectorDrawable(@NonNull Context context,
                                                      @DrawableRes int drawableId) {
        final Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        final Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * Starts a dialer app (if it exists) and provides it the given phone number.
     *
     * @param context     the context from where the function is being called.
     * @param phoneNumber the phone number to send to the dialer app.
     */
    public static void startDialer(@NonNull Context context, @NonNull String phoneNumber) {
        try {
            final String uri = "tel:" + phoneNumber;
            final Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(uri));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            FancyToast.makeText(context.getApplicationContext(),
                    context.getString(R.string.error_no_dialer),
                    FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }

    /**
     * Starts an intent to share text via apps that support it.
     *
     * @param context the context from where the function is being called.
     * @param text    the text to share
     */
    public static void startShareIntent(@NonNull Context context, @NonNull String text) {
        final Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text + "\n\n" + context.getString(R.string.signature));
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
    }

    /**
     * Starts the Place Picker activity from the Google Places SDK for Android.
     *
     * @param activity        the activity from where the function is being called.
     * @param viewForSnackbar A view context for the snackbar
     *                        (shows an error message if Google Play Services aren't found on device)
     * @param requestCode     the request code for the onActivityResult function of the given activity.
     */
    public static void startPlacePickerForResult(@NonNull Activity activity,
                                                 @NonNull View viewForSnackbar,
                                                 int requestCode) {
        try {
            final PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            activity.startActivityForResult(builder.build(activity), requestCode);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            MaterialSnackbar.make(viewForSnackbar,
                    R.string.error_play_services_not_available, Snackbar.LENGTH_LONG).show();
            Crashlytics.logException(e);
        }
    }

    /**
     * /**
     * Starts the Places Autocomplete activity from the Google Places SDK for Android.
     *
     * @param activity        the activity from where the function is being called.
     * @param viewForSnackbar A view context for the Snackbar
     *                        (shows an error message if Google Play Services aren't found on device)
     * @param requestCode     the request code for the onActivityResult function of the given activity.
     * @param mode            the mode in which the activity will open (Fullscreen or Overlay)
     */
    public static void startPlacesAutocompleteForResult(@NonNull Activity activity,
                                                        @NonNull View viewForSnackbar,
                                                        int requestCode,
                                                        int mode) {
        try {
            final PlaceAutocomplete.IntentBuilder builder = new PlaceAutocomplete.IntentBuilder(mode);
            activity.startActivityForResult(builder.build(activity), requestCode);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            MaterialSnackbar.make(viewForSnackbar,
                    R.string.error_play_services_not_available, Snackbar.LENGTH_LONG).show();
            Crashlytics.logException(e);
        }
    }

    public static PendingIntent getPendingIntentForClockApp(@NonNull Context context) {
        final Intent clockIntent = new Intent(AlarmClock.ACTION_SHOW_ALARMS);
        clockIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(context, 0, clockIntent, 0);
    }

    public static PendingIntent getPendingIntentForMainActivity(@NonNull Context context) {
        final Intent activityIntent = new Intent(context, MainActivity.class);
        activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(context, 0, activityIntent, 0);
    }

}
