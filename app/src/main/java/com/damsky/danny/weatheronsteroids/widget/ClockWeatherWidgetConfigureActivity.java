package com.damsky.danny.weatheronsteroids.widget;

import android.appwidget.AppWidgetManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.ui.activity.adapter.DrawerRecyclerAdapter;
import com.shashank.sony.fancytoastlib.FancyToast;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The configuration screen for the {@link ClockWeatherWidget ClockWeatherWidget} AppWidget.
 */
public final class ClockWeatherWidgetConfigureActivity extends AppCompatActivity
        implements DrawerRecyclerAdapter.Listener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private WeatherLocationViewModel weatherLocationViewModel;
    private DrawerRecyclerAdapter adapter;

    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        AppCompatDelegate.setDefaultNightMode(PreferencesHelper.getInstance().getNightMode());
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        setContentView(R.layout.clock_weather_widget_configure);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        configureAppWidgetId();
        if (activityInstanceIsValid()) {
            weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
            inflateRecyclerView();
        } else {
            finish();
        }
    }

    private void configureAppWidgetId() {
        final Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
    }

    private boolean activityInstanceIsValid() {
        return mAppWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID;
    }

    private void inflateRecyclerView() {
        adapter = new DrawerRecyclerAdapter();
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        setItemsObserver();
    }

    private void setItemsObserver() {
        weatherLocationViewModel.getAllAdded().observe(this, optimizedLocationItems -> {
            if (optimizedLocationItems != null && optimizedLocationItems.size() > 0) {
                adapter.submitList(optimizedLocationItems);
            } else {
                FancyToast.makeText(getApplicationContext(),
                        getString(R.string.error_widgets_no_location),
                        FancyToast.LENGTH_LONG, FancyToast.INFO, false).show();
                finish();
            }
        });
    }

    @Override
    public void onNavigationDrawerItemClicked(@NonNull OptimizedLocationItem location) {
        PreferencesHelper.getInstance().putWidgetLocationId(mAppWidgetId, location.getLocation().getId());
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        ClockWeatherWidget.updateAppWidget(this, appWidgetManager, mAppWidgetId);
        finishSuccessfully();
    }

    private void finishSuccessfully() {
        final Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }
}

