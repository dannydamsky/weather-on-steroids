package com.damsky.danny.weatheronsteroids.data.weather.model;

/**
 * This class is contained inside of {@link YahooChannel}.
 * It contains information about the wind's properties for a location.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 */
public final class YahooWind {

    private final int chill;
    private final int direction;
    private final double speed;

    public YahooWind(int chill, int direction, double speed) {
        this.chill = chill;
        this.direction = direction;
        this.speed = speed;
    }

    public int getChill() {
        return chill;
    }

    public int getDirection() {
        return direction;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "YahooWind{" +
                "chill=" + chill +
                ", direction=" + direction +
                ", speed=" + speed +
                '}';
    }
}
