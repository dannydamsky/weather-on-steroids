package com.damsky.danny.weatheronsteroids.util;

import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;

import java.util.Calendar;

public final class Time {
    private static final Time instance = new Time();

    public static Time getInstance() {
        instance.performUpdate();
        return instance;
    }

    private String amPm;
    private String hours;
    private String minutes;

    private Time() {

    }

    private void performUpdate() {
        final Calendar calendar = Calendar.getInstance();
        final int hours;
        if (PreferencesHelper.getInstance().isMetricUnits()) {
            this.amPm = "";
            hours = calendar.get(Calendar.HOUR_OF_DAY);
        } else {
            this.amPm = calendar.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
            hours = calendar.get(Calendar.HOUR);
        }
        final int minutes = calendar.get(Calendar.MINUTE);
        this.hours = hours < 10 ? "0" + hours : "" + hours;
        this.minutes = minutes < 10 ? "0" + minutes : "" + minutes;
    }

    public String getAmPm() {
        return amPm;
    }

    public String getHours() {
        return hours;
    }

    public String getMinutes() {
        return minutes;
    }

    @Override
    public String toString() {
        return "Time: " + hours + ":" + minutes + " " + amPm;
    }
}
