package com.damsky.danny.weatheronsteroids.util;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

/**
 * Utilities class that holds a single instance of Gson that is used throughout the application
 * using functions provided in this class.
 *
 * @author Danny Damsky
 */
public final class GsonUtils {

    private static final Gson gson = new Gson();

    public static <T> T fromJson(@NonNull String json, @NonNull Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    private GsonUtils() {
    }
}
