package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.damsky.danny.weatheronsteroids.data.weather.config.WeatherConditionCode;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooCondition;

import java.util.Objects;

/**
 * This class is contained inside {@link OptimizedLocationItem}.
 * It contains the weather information for the current day.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see YahooCondition
 * @see WeatherConditionCode
 */
public final class OptimizedConditionItem {

    private final int currentTemp;
    @StringRes
    private final int conditionId;
    @DrawableRes
    private final int conditionImageId;

    OptimizedConditionItem(@NonNull YahooCondition condition) {
        this.currentTemp = condition.getCurrentTemp();
        if (condition.getConditionCode() == WeatherConditionCode.NOT_AVAILABLE.getCode()) {
            this.conditionId = WeatherConditionCode.NOT_AVAILABLE.getConditionId();
            this.conditionImageId = WeatherConditionCode.NOT_AVAILABLE.getConditionImageId();
        } else {
            final WeatherConditionCode code = WeatherConditionCode.values()[condition.getConditionCode()];
            this.conditionId = code.getConditionId();
            this.conditionImageId = code.getConditionImageId();
        }
    }

    public int getCurrentTemp() {
        return currentTemp;
    }

    @StringRes
    public int getConditionId() {
        return conditionId;
    }

    @DrawableRes
    public int getConditionImageId() {
        return conditionImageId;
    }

    @Override
    public String toString() {
        return "OptimizedConditionItem{" +
                "currentTemp=" + currentTemp +
                ", conditionId=" + conditionId +
                ", conditionImageId=" + conditionImageId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedConditionItem)) return false;
        final OptimizedConditionItem that = (OptimizedConditionItem) o;
        return currentTemp == that.currentTemp &&
                conditionId == that.conditionId &&
                conditionImageId == that.conditionImageId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentTemp, conditionId, conditionImageId);
    }
}
