package com.damsky.danny.weatheronsteroids.data.weather.model;

/**
 * This class is contained inside of {@link YahooResults} as a list.
 * It holds all the information about the weather for a certain location.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 */
public final class YahooChannel {

    private final YahooUnits units;
    private final YahooWind wind;
    private final YahooAtmosphere atmosphere;
    private final YahooAstronomy astronomy;
    private final YahooItem item;

    public YahooChannel(YahooUnits units, YahooWind wind, YahooAtmosphere atmosphere, YahooAstronomy astronomy, YahooItem item) {
        this.units = units;
        this.wind = wind;
        this.atmosphere = atmosphere;
        this.astronomy = astronomy;
        this.item = item;
    }

    public YahooUnits getUnits() {
        return units;
    }

    public YahooWind getWind() {
        return wind;
    }

    public YahooAtmosphere getAtmosphere() {
        return atmosphere;
    }

    public YahooAstronomy getAstronomy() {
        return astronomy;
    }

    public YahooItem getItem() {
        return item;
    }

    @Override
    public String toString() {
        return "YahooChannel{" +
                "units=" + units +
                ", wind=" + wind +
                ", atmosphere=" + atmosphere +
                ", astronomy=" + astronomy +
                ", item=" + item +
                '}';
    }
}
