package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.weather.model.YahooAtmosphere;
import com.damsky.danny.weatheronsteroids.util.TempUtils;

import java.text.DecimalFormat;
import java.util.Objects;

/**
 * This class is contained inside {@link OptimizedLocationItem}.
 * It contains information about the location's atmosphere for the current time.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see YahooAtmosphere
 * @see TempUtils
 */
public final class OptimizedAtmosphereItem {

    private final int humidity;
    private final int realFeel;
    private final String pressure;
    private final float visibility;

    OptimizedAtmosphereItem(@NonNull YahooAtmosphere atmosphere, int currentTemp, boolean isMetric) {
        this.humidity = atmosphere.getHumidity();
        if (isMetric) {
            this.realFeel = TempUtils.getRealFeelTempMetric(currentTemp, humidity);
        } else {
            this.realFeel = TempUtils.getRealFeelTempImperial(currentTemp, humidity);
        }
        this.pressure = getFormattedPressure(atmosphere.getPressure());
        this.visibility = atmosphere.getVisibility();
    }

    private String getFormattedPressure(float pressure) {
        final DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(pressure);
    }

    public int getHumidity() {
        return humidity;
    }

    public int getRealFeel() {
        return realFeel;
    }

    public String getPressure() {
        return pressure;
    }

    public float getVisibility() {
        return visibility;
    }

    @Override
    public String toString() {
        return "OptimizedAtmosphereItem{" +
                "humidity=" + humidity +
                ", realFeel=" + realFeel +
                ", pressure='" + pressure + '\'' +
                ", visibility=" + visibility +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedAtmosphereItem)) return false;
        final OptimizedAtmosphereItem that = (OptimizedAtmosphereItem) o;
        return humidity == that.humidity &&
                realFeel == that.realFeel &&
                Float.compare(that.visibility, visibility) == 0 &&
                Objects.equals(pressure, that.pressure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(humidity, realFeel, pressure, visibility);
    }
}
