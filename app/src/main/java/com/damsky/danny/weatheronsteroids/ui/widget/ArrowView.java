package com.damsky.danny.weatheronsteroids.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.damsky.danny.weatheronsteroids.R;

/**
 * A view depicting an arrow that points at a certain direction
 * (use {@link #setDegrees(float)} to change the direction)
 *
 * @author Danny Damsky
 */
public final class ArrowView extends View {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Path path = new Path();

    private float degrees = 0f;

    public ArrowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaintParams();
    }

    private void initPaintParams() {
        paint.setColor(getResources().getColor(R.color.textColorPrimary));
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3f * getResources().getDisplayMetrics().density); // 3dp
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawArrow(canvas);
    }

    private void drawArrow(Canvas canvas) {
        // Perform calculations
        final float triangleX = getHeight() / 10;
        final float triangleY = triangleX * 3;
        final float startX = getHeight() / 2;
        final float startY = getHeight() / 5;

        // Rotate canvas
        canvas.save();
        canvas.rotate(degrees, startX, startX);

        // Draw Arrow as if it was in vertical mode
        path.moveTo(startX, startY);
        path.lineTo(startX + triangleX, triangleY);
        path.lineTo(startX - triangleX, triangleY);
        path.close();
        path.moveTo(startX, triangleY);
        path.lineTo(startX, startX + startY);

        // Draw the arrow
        canvas.drawPath(path, paint);
        canvas.restore();
    }

    public void setDegrees(@FloatRange(from = 0, to = 360) float degrees) {
        this.degrees = degrees;
        invalidate();
    }
}
