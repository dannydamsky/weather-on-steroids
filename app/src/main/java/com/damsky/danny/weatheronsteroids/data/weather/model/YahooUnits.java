package com.damsky.danny.weatheronsteroids.data.weather.model;

/**
 * This class is contained inside of {@link YahooChannel}.
 * It contains the temperature unit of measurement for the channel's location. (C or F)
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 * @see YahooResults
 * @see YahooChannel
 */
public final class YahooUnits {

    private final String temperature;

    public YahooUnits(String temperature) {
        this.temperature = temperature;
    }

    public String getTemperature() {
        return temperature;
    }

    @Override
    public String toString() {
        return "YahooUnits{" +
                "temperature='" + temperature + '\'' +
                '}';
    }
}
