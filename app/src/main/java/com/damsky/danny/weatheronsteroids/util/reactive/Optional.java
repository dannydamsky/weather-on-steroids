package com.damsky.danny.weatheronsteroids.util.reactive;

import android.support.annotation.Nullable;

import java.util.NoSuchElementException;

/**
 * Mimics the Optional class in the Java Utils library.
 * The reason for mimicking and already existing class is due to the fact that java.util.Optional
 * is only available for API 26 and above.
 *
 * @author Danny Damsky
 * @see java.util.Optional
 */
public final class Optional<T> {

    private final T optional;

    public Optional(@Nullable T optional) {
        this.optional = optional;
    }

    public boolean isEmpty() {
        return this.optional == null;
    }

    public T get() {
        if (optional == null) {
            throw new NoSuchElementException("No value present");
        }
        return optional;
    }

    @Override
    public String toString() {
        return "" + optional;
    }
}
