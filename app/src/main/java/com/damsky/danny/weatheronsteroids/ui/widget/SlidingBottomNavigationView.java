package com.damsky.danny.weatheronsteroids.ui.widget;

import android.content.Context;
import android.support.design.widget.BottomNavigationView;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.damsky.danny.weatheronsteroids.R;

/**
 * A {@link BottomNavigationView} with {@link #show()} and {@link #hide()} functionality.
 *
 * @author Danny Damsky
 */
public final class SlidingBottomNavigationView extends BottomNavigationView {

    private final Animation slideDownAnimation;
    private final Animation slideUpAnimation;

    public SlidingBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        slideDownAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down_medium);
        slideUpAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up_medium);
        slideDownAnimation.setAnimationListener(getSlideDownAnimationListener());
        slideUpAnimation.setAnimationListener(getSlideUpAnimationListener());
    }

    private Animation.AnimationListener getSlideDownAnimationListener() {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    private Animation.AnimationListener getSlideUpAnimationListener() {
        return new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
    }

    public void hide() {
        if (getVisibility() == VISIBLE) {
            startAnimation(slideDownAnimation);
        }
    }

    public void show() {
        if (getVisibility() != VISIBLE) {
            startAnimation(slideUpAnimation);
        }
    }
}
