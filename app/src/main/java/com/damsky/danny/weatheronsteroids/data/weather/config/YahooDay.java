package com.damsky.danny.weatheronsteroids.data.weather.config;

import android.support.annotation.StringRes;

import com.damsky.danny.weatheronsteroids.R;

/**
 * Days in the response from the YAHOO Weather API are represented by the first three letters
 * of the name of the day in English, with the first letter being a capital letter.
 * This Enum class contains a single parameter with a reference to the String resource
 * that contains the full name of the day.
 *
 * @author Danny Damsky
 */
public enum YahooDay {
    Sun(R.string.sunday),
    Mon(R.string.monday),
    Tue(R.string.tuesday),
    Wed(R.string.wednesday),
    Thu(R.string.thursday),
    Fri(R.string.friday),
    Sat(R.string.saturday);

    @StringRes
    private final int fullName;

    YahooDay(@StringRes int fullName) {
        this.fullName = fullName;
    }

    @StringRes
    public int getFullName() {
        return fullName;
    }
}
