package com.damsky.danny.weatheronsteroids.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.damsky.danny.weatheronsteroids.R;

/**
 * A FloatingActionButton that allows edge-to-edge dragging based on the most convenient dragging path.
 *
 * @author Danny Damsky
 */
public final class DraggableFloatingActionButton extends FloatingActionButton
        implements View.OnTouchListener {

    public static final float DEFAULT_MAX_VALUE = 20f;

    private static final int STATE_HORIZONTAL_LTR = 0;
    private static final int STATE_HORIZONTAL_RTL = 1;
    private static final int STATE_VERTICAL_BTT = 2;
    private static final int STATE_VERTICAL_TTB = 3;

    private static final int ID_NOT_FOUND = -1;
    private static final int FAB_SIZE_DP = 56;
    private static final float DEFAULT_MARGIN = 0f;

    @IdRes
    private int overlayLayoutId;
    private ViewGroup overlayLayout;

    @IdRes
    private int parentLayoutId;
    private ViewGroup parentLayout;

    private float layoutMargin;
    private float fabSize;
    private float halfFabSize;
    private float doubleFabSize;
    private float initialPositionMinusMargin;

    private float maxValue;
    private float initialPosition;
    private float currentPosition;
    private float maxPosition;

    private int movementState;

    private Animation fadeIn;
    private Animation fadeOut;
    private OnProgressChangeListener listener;

    public DraggableFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        final TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DraggableFloatingActionButton, 0, 0);
        try {
            findResources(a);
        } finally {
            a.recycle();
        }
        init();
    }

    private void findResources(TypedArray a) {
        maxValue = a.getFloat(R.styleable.DraggableFloatingActionButton_maxValue, DEFAULT_MAX_VALUE);
        parentLayoutId = a.getResourceId(R.styleable.DraggableFloatingActionButton_parentLayout, ID_NOT_FOUND);
        overlayLayoutId = a.getResourceId(R.styleable.DraggableFloatingActionButton_overlayLayout, ID_NOT_FOUND);
        layoutMargin = a.getDimension(R.styleable.DraggableFloatingActionButton_android_layout_margin, DEFAULT_MARGIN);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (overlayLayout == null && overlayLayoutId != ID_NOT_FOUND) {
            setOverlayLayout();
        } else if (parentLayout == null && parentLayoutId != ID_NOT_FOUND) {
            setParentLayout();
        } else if (parentLayout == null && overlayLayout == null) {
            throw new IllegalArgumentException(getClass().getName() +
                    " must have either a parentLayout reference or a overlayLayout reference.");
        }
    }

    private void setOverlayLayout() {
        overlayLayout = getRootView().findViewById(overlayLayoutId);
        overlayLayout.setVisibility(VISIBLE);
        setupWithLayout(overlayLayout);
        // post() functions make sure that both overlayLayout and this layout are drawn on the screen
        // before starting to configure their behavior.
        overlayLayout.post(() -> post(() -> {
            setMaxLength(overlayLayout);
            overlayLayout.setVisibility(GONE);
        }));
    }

    private void setupWithLayout(ViewGroup layout) {
        this.overlayLayout = layout;
        setFadeInAnimation();
        setFadeOutAnimation();
    }

    private void setParentLayout() {
        parentLayout = getRootView().findViewById(parentLayoutId);
        post(() -> setMaxLength(parentLayout));
    }

    private void setMaxLength(final ViewGroup viewGroup) {
        fabSize = getResources().getDisplayMetrics().density * FAB_SIZE_DP;
        if (viewGroup.getWidth() > viewGroup.getHeight()) { // Vertical state
            setMaxLengthVertical(viewGroup);
        } else { // Horizontal state
            setMaxLengthHorizontal(viewGroup);
        }
    }

    private void setMaxLengthVertical(final ViewGroup viewGroup) {
        if (viewGroup.getHeight() - getY() > getY()) { // Fab is at the top
            movementState = STATE_VERTICAL_TTB;
            setMaxLengthTtb(viewGroup);
        } else { // Fab is at the bottom
            movementState = STATE_VERTICAL_BTT;
            setMaxLengthBtt();
        }
    }

    private void setMaxLengthTtb(final ViewGroup viewGroup) {
        this.maxPosition = viewGroup.getHeight() - fabSize - layoutMargin;
        this.initialPosition = getY();
        this.doubleFabSize = fabSize * 2;
    }

    private void setMaxLengthBtt() {
        this.maxPosition = layoutMargin;
        this.initialPosition = getY();
        this.initialPositionMinusMargin = initialPosition - layoutMargin;
        this.doubleFabSize = fabSize * 2;
    }

    private void setMaxLengthHorizontal(final ViewGroup viewGroup) {
        if (viewGroup.getWidth() - getX() > getX()) { // Fab is on the left
            movementState = STATE_HORIZONTAL_LTR;
            setMaxLengthLtr(viewGroup);
        } else { // Fab is on the right
            movementState = STATE_HORIZONTAL_RTL;
            setMaxLengthRtl();
        }
    }

    private void setMaxLengthLtr(final ViewGroup viewGroup) {
        this.maxPosition = viewGroup.getWidth() - fabSize - layoutMargin;
        this.initialPosition = getX();
    }

    private void setMaxLengthRtl() {
        this.maxPosition = layoutMargin;
        this.initialPosition = getX();
        this.initialPositionMinusMargin = initialPosition - layoutMargin;
        this.halfFabSize = fabSize / 2;
    }

    private void init() {
        setSize(FloatingActionButton.SIZE_MINI);
        setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onActionDown();
                break;
            case MotionEvent.ACTION_MOVE:
                onActionMove(event);
                break;
            case MotionEvent.ACTION_UP:
                onActionUp();
                break;
            default:
                return false;
        }
        return true;
    }

    private void onActionDown() {
        this.currentPosition = this.initialPosition;
        if (listener != null) {
            listener.onTrackingStarted();
        }
        if (overlayLayout != null) {
            overlayLayout.startAnimation(fadeIn);
        }
        setSize(FloatingActionButton.SIZE_NORMAL);
    }

    private void onActionMove(MotionEvent event) {
        switch (movementState) {
            case STATE_HORIZONTAL_LTR:
                onActionMoveHorizontalLtr(event);
                break;
            case STATE_HORIZONTAL_RTL:
                onActionMoveHorizontalRtl(event);
                break;
            case STATE_VERTICAL_BTT:
                onActionMoveVerticalBtt(event);
                break;
            case STATE_VERTICAL_TTB:
                onActionMoveVerticalTtb(event);
                break;
        }
    }

    private void onActionMoveHorizontalLtr(final MotionEvent event) {
        if (event.getRawX() > initialPosition && event.getRawX() <= maxPosition) {
            this.currentPosition = event.getRawX();
            if (listener != null) {
                listener.onProgressChanged(getProgressLtrOrTtb());
            }
            animate().x(currentPosition).setDuration(0).start();
        }
    }

    private void onActionMoveHorizontalRtl(final MotionEvent event) {
        final float rawX = event.getRawX() - halfFabSize;
        if (rawX < initialPositionMinusMargin && rawX >= maxPosition) {
            this.currentPosition = rawX;
            if (listener != null) {
                listener.onProgressChanged(getProgressRtlOrBtt());
            }
            animate().x(currentPosition).setDuration(0).start();
        }
    }

    private void onActionMoveVerticalBtt(final MotionEvent event) {
        final float rawY = event.getRawY() - doubleFabSize;
        if (rawY < initialPositionMinusMargin && rawY >= maxPosition) {
            this.currentPosition = rawY;
            if (listener != null) {
                listener.onProgressChanged(getProgressRtlOrBtt());
            }
            animate().y(currentPosition).setDuration(0).start();
        }
    }

    private void onActionMoveVerticalTtb(final MotionEvent event) {
        final float rawY = event.getRawY() - doubleFabSize;
        if (rawY > initialPosition && rawY <= maxPosition) {
            this.currentPosition = rawY;
            if (listener != null) {
                listener.onProgressChanged(getProgressLtrOrTtb());
            }
            animate().y(currentPosition).setDuration(0).start();
        }
    }

    private float getProgressLtrOrTtb() {
        final float maxLength = maxPosition - initialPosition;
        final float currentLength = currentPosition - initialPosition;
        final float percent = currentLength / maxLength;
        return percent * maxValue;
    }

    private float getProgressRtlOrBtt() {
        final float maxLength = initialPosition - maxPosition;
        final float currentLength = initialPosition - currentPosition;
        final float percent = currentLength / maxLength;
        return percent * maxValue;
    }

    private void onActionUp() {
        if (movementState == STATE_HORIZONTAL_RTL || movementState == STATE_HORIZONTAL_LTR) {
            animateOnActionUpHorizontalState();
        } else {
            animateOnActionUpVerticalState();
        }
        if (overlayLayout != null) {
            overlayLayout.startAnimation(fadeOut);
        }
        setSize(FloatingActionButton.SIZE_MINI);
        if (listener != null) {
            listener.onTrackingEnded();
        }
    }

    private void animateOnActionUpHorizontalState() {
        animate().x(initialPosition)
                .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .start();

    }

    private void animateOnActionUpVerticalState() {
        animate().y(initialPosition)
                .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                .start();
    }

    private void setFadeInAnimation() {
        fadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_short);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                overlayLayout.setVisibility(VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void setFadeOutAnimation() {
        fadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out_short);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                overlayLayout.setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void setOnProgressChangeListener(@NonNull OnProgressChangeListener listener) {
        this.listener = listener;
    }

    public interface OnProgressChangeListener {
        void onTrackingStarted();

        void onProgressChanged(float progress);

        void onTrackingEnded();
    }
}
