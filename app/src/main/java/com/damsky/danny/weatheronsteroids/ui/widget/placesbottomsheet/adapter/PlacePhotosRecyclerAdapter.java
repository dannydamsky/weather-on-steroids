package com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.adapter;

import android.annotation.SuppressLint;
import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.damsky.danny.weatheronsteroids.GlideApp;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.util.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * An adapter for the Photos RecyclerView in {@link com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.PlacesBottomSheetView}.
 * This adapter implements paging functionality in order to improve performance.
 *
 * @author Danny Damsky
 */
public final class PlacePhotosRecyclerAdapter extends PagedListAdapter<LocationPhoto, PlacePhotosRecyclerAdapter.ViewHolder> {

    private Context context;

    public PlacePhotosRecyclerAdapter() {
        super(LocationPhoto.DIFF_CALLBACK);
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        final View itemView = LayoutInflater.from(context)
                .inflate(R.layout.col_place_photos, viewGroup, false);
        return new ViewHolder(itemView);
    }

    /**
     * the result in the .map lambda function will definitely be a bitmap.
     */
    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final LocationPhoto photo = getItem(i);
        if (photo != null) {
            Observable.just(photo.getBitmap())
                    .map(ImageUtils::stringToBitmap)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(bitmap -> GlideApp.with(context)
                            .load(bitmap)
                            .transform(new RoundedCorners(16))
                            .into(viewHolder.image));

        }
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
