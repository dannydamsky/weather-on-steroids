package com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedForecastItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter for the daily weather RecyclerView in {@link com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.PlacesBottomSheetView}.
 *
 * @author Danny Damsky
 */
public final class PlacesWeatherRecyclerAdapter extends RecyclerView.Adapter<PlacesWeatherRecyclerAdapter.ViewHolder> {

    private final OptimizedForecastItem[] forecast;
    private final String tempUnit;
    private Context context;

    public PlacesWeatherRecyclerAdapter(@NonNull OptimizedLocationItem locationItem) {
        this.forecast = locationItem.getForecast();
        this.tempUnit = locationItem.getTempUnit();
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return forecast[position].hashCode();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        final View itemView = LayoutInflater.from(context)
                .inflate(R.layout.col_weather_daily, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final OptimizedForecastItem item = forecast[i];
        setTemps(viewHolder, item);
        viewHolder.mainTextView.setText(item.getDayId());
        viewHolder.secondaryTextView.setText(item.getConditionId());
        viewHolder.weatherImage.setImageResource(item.getConditionImageId());
    }

    private void setTemps(ViewHolder viewHolder, OptimizedForecastItem item) {
        viewHolder.dayTempTextView.setText(context.getString(R.string.degree, item.getDayTemp()) + tempUnit);
        if (TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) == View.LAYOUT_DIRECTION_LTR) {
            viewHolder.nightTempTextView.setText(" / " + context.getString(R.string.degree, item.getNightTemp()) + tempUnit);
        } else {
            viewHolder.nightTempTextView.setText(context.getString(R.string.degree, item.getNightTemp()) + tempUnit + " / ");
        }
    }

    @Override
    public int getItemCount() {
        return forecast.length;
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.weatherImage)
        ImageView weatherImage;
        @BindView(R.id.mainTextView)
        TextView mainTextView;
        @BindView(R.id.secondaryTextView)
        TextView secondaryTextView;
        @BindView(R.id.dayTempTextView)
        TextView dayTempTextView;
        @BindView(R.id.nightTempTextView)
        TextView nightTempTextView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
