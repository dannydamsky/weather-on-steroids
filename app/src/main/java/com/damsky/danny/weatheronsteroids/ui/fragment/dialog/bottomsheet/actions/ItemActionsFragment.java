package com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.actions;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.damsky.danny.weatheronsteroids.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * This fragment contains a BottomSheetDialog with options to rename a location and
 * remove a location.
 * <br /><br />
 * Make sure to implement {@link Listener} when using this class.
 *
 * @author Danny Damsky
 */
public final class ItemActionsFragment extends BottomSheetDialogFragment {

    /**
     * Shows the dialog.
     *
     * @param fragmentManager the fragment manager that should hold the dialog.
     */
    public static void show(@NonNull FragmentManager fragmentManager) {
        final ItemActionsFragment itemActionsFragment = new ItemActionsFragment();
        itemActionsFragment.show(fragmentManager, TAG);
    }

    private static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.actions.ItemActionsFragment.TAG";

    private Context context;
    private Listener listener;
    private Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        listener = (Listener) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.fragment_item_actions, container, false);
        unbinder = ButterKnife.bind(this, layout);
        return layout;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog d = super.onCreateDialog(savedInstanceState);
        d.setOnShowListener(dialogInterface -> {
            final BottomSheetDialog dialog = (BottomSheetDialog) dialogInterface;
            final FrameLayout bottomSheet = dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        return d;
    }

    @OnClick(R.id.editTextView)
    public void onEditPressed() {
        listener.onEditPressed();
        dismiss();
    }

    @OnClick(R.id.deleteTextView)
    public void onDeletePressed() {
        listener.onDeletePressed();
        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface Listener {
        void onEditPressed();

        void onDeletePressed();
    }
}
