package com.damsky.danny.weatheronsteroids.data.auth;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.damsky.danny.weatheronsteroids.R;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Collections;

/**
 * A helper class for Firebase Authentication work
 *
 * @author Danny Damsky
 */
public final class AuthHelper {

    public static boolean isLoggedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    public static String getDisplayName() {
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    }

    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Nullable
    public static String getPhotoUrl() {
        final Uri photoUri = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl();
        if (photoUri != null) {
            return photoUri.toString();
        }
        return null;
    }

    public static void startGoogleLoginIntent(@NonNull Activity activity, int requestCode) {
        activity.startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                .setTheme(R.style.AppTheme_NoActionBar)
                .setIsSmartLockEnabled(true)
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.GoogleBuilder().build())).build(), requestCode);
    }

    public static void logOut() {
        FirebaseAuth.getInstance().signOut();
    }

    private AuthHelper() {
    }
}
