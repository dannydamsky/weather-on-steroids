package com.damsky.danny.weatheronsteroids.ui.fragment.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.receiver.WeatherUpdatesReceiver;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert.CustomizableDialogFragment;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert.DialogFragmentBundle;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;

/**
 * This fragment is used to change settings that affect how the app works.
 * It's contained inside {@link com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment}.
 *
 * @author Danny Damsky.
 */
public final class PreferencesFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.prefs.PreferencesFragment.TAG";

    private MainActivity activity;
    private Preference nightModePreference;
    private Preference refreshPreference;
    private Preference autoUpdatePreference;
    private Preference updateSchedulePreference;
    private Preference systemOfMeasurementPreference;
    private Preference clearDataPreference;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        registerPreferences(s);
        findPreferences();
        setSummaries();
        setClearDataPreferenceOnClick();
    }


    private void registerPreferences(String s) {
        setPreferencesFromResource(R.xml.preferences, s);
        PreferencesHelper.getInstance().registerOnSharedPreferenceChangeListener(this);
    }

    private void findPreferences() {
        nightModePreference = findPreference(PreferencesHelper.KEY_NIGHT_MODE);
        refreshPreference = findPreference(PreferencesHelper.KEY_REFRESH_WHEN_OPENED);
        autoUpdatePreference = findPreference(PreferencesHelper.KEY_AUTO_UPDATE);
        updateSchedulePreference = findPreference(PreferencesHelper.KEY_UPDATE_SCHEDULE);
        systemOfMeasurementPreference = findPreference(PreferencesHelper.KEY_SYSTEM_OF_MEASUREMENT);
        clearDataPreference = findPreference(PreferencesHelper.KEY_CLEAR_DATA);
    }

    private void setSummaries() {
        final PreferencesHelper instance = PreferencesHelper.getInstance();
        setNightModeSummary(instance);
        setRefreshSummary(instance);
        setAutoUpdateSummary(instance);
        setUpdateScheduleSummary(instance);
        setSystemOfMeasurementSummary(instance);
    }

    private void setNightModeSummary(PreferencesHelper instance) {
        if (instance.isNightModeEnabled()) {
            nightModePreference.setSummary(R.string.summary_night_mode_disable);
        } else {
            nightModePreference.setSummary(R.string.summary_night_mode_enable);
        }
    }

    private void setRefreshSummary(PreferencesHelper instance) {
        if (instance.isRefreshWhenOpenedEnabled()) {
            refreshPreference.setSummary(R.string.summary_refresh_disabled);
        } else {
            refreshPreference.setSummary(R.string.summary_refresh_enabled);
        }
    }

    private void setAutoUpdateSummary(PreferencesHelper instance) {
        if (instance.isAutoUpdateEnabled()) {
            autoUpdatePreference.setSummary(R.string.summary_auto_update_disabled);
        } else {
            autoUpdatePreference.setSummary(R.string.summary_auto_update_enabled);
        }
    }

    private void setUpdateScheduleSummary(PreferencesHelper instance) {
        switch (instance.getUpdateScheduleInHours()) {
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_ONE_HOUR:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_one_hour);
                break;
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_TWO_HOURS:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_two_hours);
                break;
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_THREE_HOURS:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_three_hours);
                break;
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_SIX_HOURS:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_six_hours);
                break;
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_TWELVE_HOURS:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_twelve_hours);
                break;
            case PreferencesHelper.VALUE_UPDATE_SCHEDULE_ONE_DAY:
                updateSchedulePreference.setSummary(R.string.summary_update_schedule_day);
                break;
        }
    }

    private void setSystemOfMeasurementSummary(PreferencesHelper instance) {
        if (instance.isMetricUnits()) {
            systemOfMeasurementPreference.setSummary(R.string.summary_system_of_measurement_metric);
        } else {
            systemOfMeasurementPreference.setSummary(R.string.summary_system_of_measurement_imperial);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        switch (s) {
            case PreferencesHelper.KEY_NIGHT_MODE:
                setNightModeSummary(PreferencesHelper.getInstance());
                activity.recreate();
                break;
            case PreferencesHelper.KEY_REFRESH_WHEN_OPENED:
                setRefreshSummary(PreferencesHelper.getInstance());
                break;
            case PreferencesHelper.KEY_AUTO_UPDATE:
                setAutoUpdateSummary(PreferencesHelper.getInstance());
                WeatherUpdatesReceiver.notifyDataChanged(activity);
                break;
            case PreferencesHelper.KEY_UPDATE_SCHEDULE:
                setUpdateScheduleSummary(PreferencesHelper.getInstance());
                WeatherUpdatesReceiver.notifyDataChanged(activity);
                break;
            case PreferencesHelper.KEY_SYSTEM_OF_MEASUREMENT:
                setSystemOfMeasurementSummary(PreferencesHelper.getInstance());
                DbOpsWorker.updateAllPlaces(activity);
                break;
        }
    }

    private void setClearDataPreferenceOnClick() {
        clearDataPreference.setOnPreferenceClickListener(preference -> {
            if (getFragmentManager() != null) {
                CustomizableDialogFragment.show(getFragmentManager(), getDialogFragmentBundle());
            }
            return false;
        });
    }

    private DialogFragmentBundle getDialogFragmentBundle() {
        return new DialogFragmentBundle.Builder()
                .setTitle(getString(R.string.delete_all_title))
                .setMessage(getString(R.string.delete_all_message))
                .setPositiveText(getString(R.string.delete))
                .setNegativeText(getString(R.string.cancel))
                .setFragmentTag(TAG)
                .build();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PreferencesHelper.getInstance().unregisterOnSharedPreferenceChangeListener(this);
    }
}
