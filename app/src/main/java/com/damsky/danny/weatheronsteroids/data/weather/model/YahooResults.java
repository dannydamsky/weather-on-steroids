package com.damsky.danny.weatheronsteroids.data.weather.model;

import com.damsky.danny.weatheronsteroids.util.gsonfix.AlwaysListTypeAdapterFactory;
import com.google.gson.annotations.JsonAdapter;

import java.util.List;

/**
 * The base class that gets converted from the YAHOO response JSON using Gson.
 * This class contains a list of channels (Weather results)
 * which contain weather information about a location.
 * These classes should only be instantiated with Gson!
 *
 * @author Danny Damsky
 */
public final class YahooResults {

    @JsonAdapter(AlwaysListTypeAdapterFactory.class)
    private final List<YahooChannel> channel;

    public YahooResults(List<YahooChannel> channel) {
        this.channel = channel;
    }

    public List<YahooChannel> getChannel() {
        return channel;
    }

    @Override
    public String toString() {
        return "YahooResults{" +
                "channel=" + channel +
                '}';
    }
}
