package com.damsky.danny.weatheronsteroids.customtabs;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.damsky.danny.weatheronsteroids.util.Constants;

/**
 * Default {@link CustomTabsHelper.CustomTabFallback} implementation
 * that uses {@link WebViewActivity} to display the requested {@link Uri}.
 *
 * @author Sascha Peilicke
 * @see <a href="https://github.com/saschpe/android-customtabs">Android CustomTabs Library</a>
 */
public final class WebViewFallback implements CustomTabsHelper.CustomTabFallback {
    /**
     * @param context The {@link Context} that wants to open the Uri
     * @param uri     The {@link Uri} to be opened by the fallback
     */
    @Override
    public void openUri(final Context context, final Uri uri) {
        final Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(Constants.EXTRA_URL, uri.toString());
        context.startActivity(intent);
    }
}