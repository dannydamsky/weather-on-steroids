package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooChannel;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooForecast;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooItem;
import com.damsky.danny.weatheronsteroids.data.weather.model.YahooResults;
import com.damsky.danny.weatheronsteroids.util.GsonUtils;

import java.util.Arrays;
import java.util.Objects;

/**
 * The parent class of all other classes under this package.
 * The purpose of these "optimized" classes is to provide the results in post-processed form,
 * so that there will not be any overhead when binding the data to the UI elements.
 * <br />
 * This class contains all of the data from {@link WeatherLocation} and from the YAHOO Weather API
 * classes.
 *
 * @author Danny Damsky
 * @see WeatherLocation
 * @see YahooResults
 */
public final class OptimizedLocationItem {

    public static final DiffUtil.ItemCallback<OptimizedLocationItem> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<OptimizedLocationItem>() {
                @Override
                public boolean areItemsTheSame(@NonNull OptimizedLocationItem optimizedLocationItem,
                                               @NonNull OptimizedLocationItem t1) {
                    return optimizedLocationItem.location.getId().equals(t1.location.getId());
                }

                @Override
                public boolean areContentsTheSame(@NonNull OptimizedLocationItem optimizedLocationItem,
                                                  @NonNull OptimizedLocationItem t1) {
                    return optimizedLocationItem.equals(t1);
                }
            };

    private final OptimizedWeatherLocation location;
    private final String tempUnit;
    private final boolean isMetric;
    private final OptimizedConditionItem condition;
    private final OptimizedAtmosphereItem atmosphere;
    private final OptimizedWindItem wind;
    private final OptimizedAstronomyItem astronomy;
    private final OptimizedForecastItem[] forecast;

    /**
     * Pre-loads all data from the passed object into ready-for-use fields.
     * (WeatherLocation contains the JSON for the Yahoo Weather API classes, it gets parsed here)
     *
     * @param location the location to post-process. (all fields must be non-null)
     */
    public OptimizedLocationItem(@NonNull WeatherLocation location) {
        this.location = new OptimizedWeatherLocation(location);
        final YahooChannel channel = getYahooChannel(location.getLastKnownResponse());
        this.tempUnit = channel.getUnits().getTemperature();
        this.isMetric = tempUnit.equals("C");
        final YahooItem item = channel.getItem();
        this.condition = new OptimizedConditionItem(item.getCondition());
        this.forecast = getOptimizedForecasts(item);
        this.atmosphere = new OptimizedAtmosphereItem(channel.getAtmosphere(), this.condition.getCurrentTemp(), isMetric);
        this.wind = new OptimizedWindItem(channel.getWind(), isMetric);
        this.astronomy = new OptimizedAstronomyItem(channel.getAstronomy(), isMetric);
    }

    private YahooChannel getYahooChannel(@NonNull String json) {
        final YahooResults results = GsonUtils.fromJson(json, YahooResults.class);
        return results.getChannel().get(0);
    }

    private OptimizedForecastItem[] getOptimizedForecasts(@NonNull YahooItem item) {
        final YahooForecast[] forecasts = item.getForecast();
        final int length = forecasts.length;
        final OptimizedForecastItem[] items = new OptimizedForecastItem[length];
        for (int i = 0; i < length; i++) {
            items[i] = new OptimizedForecastItem(forecasts[i]);
        }
        return items;
    }

    public OptimizedWeatherLocation getLocation() {
        return location;
    }

    public String getTempUnit() {
        return tempUnit;
    }

    public boolean isMetric() {
        return isMetric;
    }

    public OptimizedConditionItem getCondition() {
        return condition;
    }

    public OptimizedAtmosphereItem getAtmosphere() {
        return atmosphere;
    }

    public OptimizedWindItem getWind() {
        return wind;
    }

    public OptimizedAstronomyItem getAstronomy() {
        return astronomy;
    }

    public OptimizedForecastItem[] getForecast() {
        return forecast;
    }

    @Override
    public String toString() {
        return "OptimizedLocationItem{" +
                "location=" + location +
                ", tempUnit='" + tempUnit + '\'' +
                ", isMetric=" + isMetric +
                ", condition=" + condition +
                ", atmosphere=" + atmosphere +
                ", wind=" + wind +
                ", astronomy=" + astronomy +
                ", forecast=" + Arrays.toString(forecast) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedLocationItem)) return false;
        final OptimizedLocationItem that = (OptimizedLocationItem) o;
        return isMetric == that.isMetric &&
                Objects.equals(location, that.location) &&
                Objects.equals(tempUnit, that.tempUnit) &&
                Objects.equals(condition, that.condition) &&
                Objects.equals(atmosphere, that.atmosphere) &&
                Objects.equals(wind, that.wind) &&
                Objects.equals(astronomy, that.astronomy) &&
                Arrays.equals(forecast, that.forecast);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(location, tempUnit, isMetric, condition, atmosphere, wind, astronomy);
        result = 31 * result + Arrays.hashCode(forecast);
        return result;
    }
}
