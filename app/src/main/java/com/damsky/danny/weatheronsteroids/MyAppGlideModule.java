package com.damsky.danny.weatheronsteroids;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

/**
 * This class is used by Glide's annotation processor to provide advanced features and
 * improved performance. (Which is why the GlideApp class is used in the app instead of Glide)
 *
 * @author Danny Damsky
 * @see <a href="https://bumptech.github.io/glide/doc/generatedapi.html">Generated API</a>
 */
@GlideModule
public final class MyAppGlideModule extends AppGlideModule {

    /**
     * Overrides applyOptions to enable Hardware Bitmaps
     *
     * @see <a href="https://bumptech.github.io/glide/doc/hardwarebitmaps.html">Hardware Bitmaps</a>
     */
    @Override
    public void applyOptions(@NonNull Context context, @NonNull GlideBuilder builder) {
        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888));
    }
}
