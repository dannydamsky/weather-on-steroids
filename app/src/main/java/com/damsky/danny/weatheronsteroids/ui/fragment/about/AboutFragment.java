package com.damsky.danny.weatheronsteroids.ui.fragment.about;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.report.ReportBugFragment;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * This fragment is contained inside {@link com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment}.
 * <br /><br />
 * The purpose of this fragment is to display information regarding the application.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see com.damsky.danny.weatheronsteroids.ui.fragment.pager.infopager.InfoPagerFragment
 */
public final class AboutFragment extends Fragment {

    @BindView(R.id.versionText)
    TextView versionText;

    private MainActivity activity;
    private Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_about, container, false);
        unbinder = ButterKnife.bind(this, layout);
        versionText.append(" " + getString(R.string.versionName));
        return layout;
    }

    @OnClick(R.id.sourceCodeText)
    public void onSourceCodeButtonClicked() {
        IntentUtils.startCustomTabs(activity, Constants.URL_SOURCE_CODE);
    }

    @OnClick(R.id.authorNameText)
    public void onAuthorNameButtonClicked() {
        if (isAppInstalled("com.linkedin.android")) {
            IntentUtils.startWebsite(activity, Constants.URL_LINKED_IN);
        } else {
            IntentUtils.startCustomTabs(activity, Constants.URL_LINKED_IN);
        }
    }

    @OnClick(R.id.rateText)
    public void onRateButtonClicked() {
        try {
            IntentUtils.startWebsite(activity, Constants.URL_STORE);
        } catch (ActivityNotFoundException e) {
            IntentUtils.startCustomTabs(activity, Constants.URL_STORE_BACKUP);
        }
    }

    @OnClick(R.id.reportText)
    public void onReportBugButtonClicked() {
        ReportBugFragment.show(activity.getSupportFragmentManager());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private boolean isAppInstalled(@NonNull String packageName) {
        try {
            final PackageManager pm = activity.getPackageManager();
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return pm.getApplicationInfo(packageName, 0).enabled;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
