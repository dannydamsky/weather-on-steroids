package com.damsky.danny.weatheronsteroids.util.yahoo;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * This class is used to built a URL that points to a query in the YAHOO Weather API.
 *
 * @author Danny Damsky
 */
public final class YahooRequestBuilder {

    private static final String YQL_PREFIX = "select * from weather.forecast where woeid";
    private static final String ENDPOINT_PREFIX = "https://query.yahooapis.com/v1/public/yql?q=";
    private static final String ENDPOINT_SUFFIX = "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

    @Nullable
    private static String getSearchRequestUrl(@NonNull String searchText) {
        final StringBuilder yqlQueryBuilder = new StringBuilder(YQL_PREFIX);
        yqlQueryBuilder.append(" in (select woeid from geo.places where text=\"")
                .append(searchText).append("\")");
        appendUnits(yqlQueryBuilder);
        return getUrl(yqlQueryBuilder);
    }

    @Nullable
    public static String getLocationRequestUrl(double latitude, double longitude) {
        return getSearchRequestUrl("(" + latitude + "," + longitude + ")");
    }

    private static void appendUnits(@NonNull StringBuilder yqlQueryBuilder) {
        if (PreferencesHelper.getInstance().isMetricUnits()) {
            yqlQueryBuilder.append(" and u='c'");
        }
    }

    @Nullable
    private static String getUrl(@NonNull StringBuilder builder) {
        try {
            return getEncodedString(builder);
        } catch (UnsupportedEncodingException e) {
            Log.e("ENCODING", e.getMessage());
            return null;
        }
    }

    private static String getEncodedString(@NonNull StringBuilder builder) throws UnsupportedEncodingException {
        final String encodedQuery = URLEncoder.encode(builder.toString(), "UTF-8");
        return ENDPOINT_PREFIX + encodedQuery + ENDPOINT_SUFFIX;
    }

    private YahooRequestBuilder() {
    }

}
