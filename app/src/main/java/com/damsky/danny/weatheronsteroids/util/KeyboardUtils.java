package com.damsky.danny.weatheronsteroids.util;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Util class that is meant to fix the keyboard covering the BottomSheetDialogFragment
 * whenever it is present.
 *
 * @author Danny Damsky
 */
public final class KeyboardUtils {

    /**
     * Sets the keyboard below the dialog.
     * HTC devices aren't affected by this issue, therefore, for an HTC device, setting "adjustResize"
     * is enough for the keyboard to not cover the dialog, so for them, this function does nothing.
     *
     * @param activity    the activity that the dialog belongs to.
     * @param contentView the layout of the dialog.
     */
    public static void setBelowDialog(@NonNull Activity activity, @NonNull View contentView) {
        if (!Build.MANUFACTURER.equals("HTC")) {
            final View decorView = activity.getWindow().getDecorView();
            decorView.getViewTreeObserver().addOnGlobalLayoutListener(getGlobalLayoutListener(decorView, contentView));
        }
    }

    private static ViewTreeObserver.OnGlobalLayoutListener getGlobalLayoutListener(@NonNull View decorView, @NonNull View contentView) {
        return () -> {
            final Rect rect = new Rect();
            decorView.getWindowVisibleDisplayFrame(rect);
            final int height = decorView.getContext().getResources().getDisplayMetrics().heightPixels;
            final int diff = height - rect.bottom;
            if (diff != 0) {
                if (contentView.getPaddingBottom() != diff) {
                    contentView.setPadding(0, 0, 0, diff);
                }
            } else {
                if (contentView.getPaddingBottom() != 0) {
                    contentView.setPadding(0, 0, 0, 0);
                }
            }
        };
    }
}