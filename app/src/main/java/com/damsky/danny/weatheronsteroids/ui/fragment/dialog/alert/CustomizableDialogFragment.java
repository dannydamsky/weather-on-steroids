package com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;

/**
 * An AlertDialog Fragment that's customized to have optional fields such as an EditText inside it.
 * This fragment uses {@link TextChangeListener} for callbacks from dialogs with EditTexts in them
 * and {@link SimpleActionListener} for callbacks from dialogs without EditTexts in them.
 * Make sure to implement the proper listener when using this class.
 *
 * @author Danny Damsky
 * @see AlertDialog
 * @see DialogFragment
 */
public final class CustomizableDialogFragment extends DialogFragment {

    private static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.dialog.alert.CustomizableDialogFragment.TAG";

    /**
     * Shows the dialog.
     *
     * @param fragmentManager the fragment manager that should hold the dialog.
     * @param bundle          a bundle containing arguments for the dialog.
     * @see DialogFragmentBundle
     */
    public static void show(@NonNull FragmentManager fragmentManager,
                            @NonNull DialogFragmentBundle bundle) {
        final CustomizableDialogFragment customizableDialogFragment = new CustomizableDialogFragment();
        customizableDialogFragment.setArguments(bundle.toBundle());
        customizableDialogFragment.show(fragmentManager, TAG);
    }

    private TextChangeListener textChangeListener;
    private SimpleActionListener simpleListener;
    private DialogFragmentBundle bundle;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        bundle = new DialogFragmentBundle(getArguments());
        if (bundle.getHint() == null) {
            simpleListener = (SimpleActionListener) context;
        } else {
            textChangeListener = (TextChangeListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogWindowAnimation;
        } catch (NullPointerException e) {
            Crashlytics.logException(e);
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(bundle.getTitle());
        setMessage(builder);
        setNegativeButton(builder);
        return buildCorrectDialog(builder);
    }

    private void setMessage(AlertDialog.Builder builder) {
        if (bundle.getMessage() != null) {
            builder.setMessage(bundle.getMessage());
        }
    }

    private void setNegativeButton(AlertDialog.Builder builder) {
        if (bundle.getNegativeText() != null) {
            builder.setNegativeButton(bundle.getNegativeText(), null);
        }
    }

    private AlertDialog buildCorrectDialog(AlertDialog.Builder builder) {
        if (bundle.getHint() != null) {
            setEditText(builder);
            return createEditableDialog(builder);
        } else {
            builder.setPositiveButton(bundle.getPositiveText(), (dialogInterface, i) ->
                    simpleListener.onDialogComplete(bundle.getFragmentTag()));
            return builder.create();
        }
    }

    private void setEditText(AlertDialog.Builder builder) {
        final EditText editText = getConfiguredEditText();
        builder.setView(buildEditTextContainer(editText))
                .setPositiveButton(bundle.getPositiveText(), (dialogInterface, i) ->
                        deployCallback(editText.getText()));
    }

    private FrameLayout buildEditTextContainer(final EditText editText) {
        final FrameLayout container = new FrameLayout(getContext());
        editText.setLayoutParams(getEditTextParams());
        container.addView(editText);
        return container;
    }

    private FrameLayout.LayoutParams getEditTextParams() {
        final float density = getResources().getDisplayMetrics().density;
        final FrameLayout.LayoutParams params =
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int dp = (int) (20 * density);
        params.leftMargin = dp;
        params.rightMargin = dp;
        return params;
    }

    private EditText getConfiguredEditText() {
        final EditText editText = new EditText(getContext());
        editText.setHint(bundle.getHint());
        final String text = bundle.getText();
        editText.setText(text);
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.setSelection(0, text.length());
        return editText;
    }

    private AlertDialog createEditableDialog(AlertDialog.Builder builder) {
        final AlertDialog dialog = builder.create();
        if (bundle.getHint() != null) {
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
        return dialog;
    }

    private void deployCallback(CharSequence text) {
        final String textString = text.toString().trim();
        if (!textString.isEmpty()) {
            textChangeListener.onEditableDialogComplete(text.toString());
        }
    }

    public interface TextChangeListener {
        void onEditableDialogComplete(@NonNull String text);
    }

    public interface SimpleActionListener {
        void onDialogComplete(@Nullable String fragmentTag);
    }
}
