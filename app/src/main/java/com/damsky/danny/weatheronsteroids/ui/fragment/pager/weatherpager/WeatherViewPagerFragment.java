package com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.adapter.WeatherPagerAdapter;
import com.damsky.danny.weatheronsteroids.util.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This fragment holds up to 3 fragments including
 * {@link com.damsky.danny.weatheronsteroids.ui.fragment.today.TodayWeatherFragment},
 * {@link com.damsky.danny.weatheronsteroids.ui.fragment.daily.DailyWeatherFragment} and
 * {@link com.damsky.danny.weatheronsteroids.ui.fragment.litemap.LiteMapFragment} (not present on tablets)
 * in a tabbed ViewPager.
 *
 * @author Danny Damsky
 */
public final class WeatherViewPagerFragment extends Fragment {

    public static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment.TAG";

    /**
     * Shows the fragment inside the parent layout.
     *
     * @param fragmentManager      the fragment manager to hold the fragment.
     * @param parentLayoutId       the layout to contain the fragment.
     * @param isLargeLayoutEnabled a boolean depicting whether the device is a tablet or not.
     * @param weatherLocationId    the ID of the location to display in the child fragments.
     */
    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId,
                            boolean isLargeLayoutEnabled,
                            @NonNull String weatherLocationId) {
        final Bundle bundle = new Bundle();
        bundle.putString(Constants.EXTRA_ID, weatherLocationId);
        bundle.putBoolean(Constants.EXTRA_IS_LARGE_LAYOUT, isLargeLayoutEnabled);
        final WeatherViewPagerFragment fragment = new WeatherViewPagerFragment();
        fragment.setArguments(bundle);
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.addToBackStack(TAG);
        transaction.setCustomAnimations(R.anim.slide_up_medium, R.anim.fade_out_short, android.R.anim.fade_in, R.anim.slide_down_short);
        transaction.replace(parentLayoutId, fragment, TAG);
        transaction.commit();
    }

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    private PagerAdapter pagerAdapter;
    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(getContext())
                .inflate(R.layout.fragment_pager, container, false);
        unbinder = ButterKnife.bind(this, layout);
        setupPager();
        return layout;
    }

    private void setupPager() {
        pager.setOffscreenPageLimit(determinePagerOffscreenLimit());
        pagerAdapter = new WeatherPagerAdapter(this);
        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);
    }

    private int determinePagerOffscreenLimit() {
        if (Constants.DEVICE_YEAR > 2013) {
            return 2;
        }
        return 1;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
