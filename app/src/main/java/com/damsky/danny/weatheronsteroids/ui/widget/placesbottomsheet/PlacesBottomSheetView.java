package com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet;

import android.arch.paging.PagedList;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.model.LocationPhoto;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedConditionItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.adapter.PlacePhotosRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.ui.widget.placesbottomsheet.adapter.PlacesWeatherRecyclerAdapter;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.shashank.sony.fancytoastlib.FancyToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * An expandable BottomSheetView that shows details regarding a certain location and its weather.
 *
 * @author Danny Damsky
 */
public final class PlacesBottomSheetView extends ConstraintLayout implements View.OnLayoutChangeListener {

    @BindView(R.id.maximizedLayoutPlaceholder)
    ProgressBar maximizedLayoutPlaceholder;
    @BindView(R.id.maximizedLayout)
    NestedScrollView maximizedLayout;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.callButton)
    Button callButton;
    @BindView(R.id.addButton)
    Button addButton;
    @BindView(R.id.websiteButton)
    Button websiteButton;
    @BindView(R.id.addressText)
    TextView addressText;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.websiteText)
    TextView websiteText;
    @BindView(R.id.weatherRecyclerView)
    RecyclerView weatherRecyclerView;
    @BindView(R.id.photosRecyclerView)
    RecyclerView photosRecyclerView;
    @BindView(R.id.locationNameText)
    TextView locationNameText;
    @BindView(R.id.tempTextPlaceholder)
    ProgressBar tempTextPlaceholder;
    @BindView(R.id.tempText)
    TextView tempText;
    @BindView(R.id.conditionTextPlaceholder)
    ProgressBar conditionTextPlaceholder;
    @BindView(R.id.conditionText)
    TextView conditionText;
    @BindView(R.id.actionButtonPlaceholder)
    ProgressBar actionButtonPlaceholder;
    @BindView(R.id.actionButton)
    Button actionButton;
    @BindView(R.id.moreInfoText)
    TextView moreInfoText;

    private final PlacePhotosRecyclerAdapter adapter = new PlacePhotosRecyclerAdapter();

    private String locationName;
    private BottomSheetBehavior bottomSheetBehavior;
    private OptimizedLocationItem locationItem;
    private boolean existsInDb;
    private Callback callback;
    private FloatingActionButton[] fabs;

    private final GeoDataClient geoDataClient;
    private Place place;

    private boolean instanceSaved = false;
    private boolean swipeUp = false;
    private int tempTextPlaceholderVisibility;
    private int tempTextVisibility;
    private int conditionTextPlaceholderVisibility;
    private int conditionTextVisibility;
    private int actionButtonPlaceholderVisibility;
    private int actionButtonVisibility;

    public PlacesBottomSheetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        final View layout = inflate(context, R.layout.widget_bottom_sheet_place, this);
        ButterKnife.bind(this, layout);
        geoDataClient = Places.getGeoDataClient(context);
        setWeatherRecyclerViewProperties();
        setPhotosRecyclerView();
    }

    private void setWeatherRecyclerViewProperties() {
        weatherRecyclerView.setHasFixedSize(true);
        weatherRecyclerView.setItemAnimator(null);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
    }

    private void setPhotosRecyclerView() {
        photosRecyclerView.setHasFixedSize(true);
        photosRecyclerView.setItemAnimator(null);
        photosRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        photosRecyclerView.setAdapter(adapter);
    }

    public void from(@NonNull PlacesBottomSheetView view) {
        bottomSheetBehavior = BottomSheetBehavior.from(view);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        addOnLayoutChangeListener(this);
        setBottomSheetCallback();
    }

    public void setFabs(@NonNull FloatingActionButton... fabs) {
        this.fabs = fabs;
    }

    private void hideFabs() {
        if (fabs != null) {
            for (FloatingActionButton fab : fabs) {
                fab.hide();
            }
        }
    }

    private void showFabs() {
        if (fabs != null) {
            for (FloatingActionButton fab : fabs) {
                fab.show();
            }
        }
    }

    @Override
    public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
        hideIfNotInflated();
    }

    @OnClick(R.id.bottomSheetParent)
    public void onLayoutClicked() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            callback.onBottomSheetExpanded(locationName);
        }
    }

    public boolean isVisible() {
        return bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN;
    }

    public void hide() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    public void show(@NonNull String locationName) {
        this.locationName = locationName;
        setMinimizedLayoutViewsInvisible();
        hideFabs();
        locationNameText.setText(locationName);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public void setCallback(@NonNull Callback callback) {
        this.callback = callback;
    }

    private void setBottomSheetCallback() {
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        onStateCollapsed();
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                    case BottomSheetBehavior.STATE_DRAGGING:
                        onStateSettlingOrDragging();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        onStateExpanded();
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        onStateHidden();
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
                setAlphaToMinimizedViews(1 - v);
                setAlphaToMaximizedViews(v);
            }
        });
    }

    private void onStateCollapsed() {
        hideFabs();
        swipeUp = true;
        restoreVisibilities();
        setMaximizedViewsGone();
        callback.onBottomSheetCollapsed();
    }

    private void onStateSettlingOrDragging() {
        if (locationItem != null) {
            restoreVisibilities();
            setMaximizedViewsVisible();
            if (swipeUp) {
                prepareMaximizedLayout();
                callback.onBottomSheetPreparingMaximizedLayout(locationItem.getLocation().getId());
            }
        } else {
            if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN && !swipeUp) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }

    private void onStateExpanded() {
        if (locationItem != null) {
            swipeUp = false;
            setMinimizedViewsGone();
            setMaximizedViewsVisible();
            callback.onBottomSheetExpanded(locationName);
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    private void onStateHidden() {
        if (!swipeUp) {
            onStateCollapsed();
        }
        swipeUp = false;
        instanceSaved = false;
        showFabs();
        setMinimizedLayoutViewsInvisible();
    }

    private void setMaximizedViewsVisible() {
        if (place != null && place.getId().equals(locationItem.getLocation().getId())) {
            inflateMaximizedLayout();
        } else {
            maximizedLayoutPlaceholder.setVisibility(VISIBLE);
        }
    }

    private void inflateMaximizedLayout() {
        maximizedLayoutPlaceholder.setVisibility(GONE);
        maximizedLayout.setVisibility(VISIBLE);
        setRatingView(place.getRating());
        setPhoneViews(place.getPhoneNumber());
        setAddressText(place.getAddress());
        setWebsiteText(place.getWebsiteUri());
        setWeatherRecyclerView();
    }

    private void setRatingView(float rating) {
        if (rating >= 1.0) {
            this.rating.setVisibility(VISIBLE);
            this.rating.setRating(place.getRating());
        } else {
            this.rating.setVisibility(GONE);
        }
    }

    private void setPhoneViews(@Nullable CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            phoneText.setVisibility(VISIBLE);
            callButton.setVisibility(VISIBLE);
            phoneText.setText(phoneNumber);
            setPhoneClickListener(phoneNumber.toString());
        } else {
            phoneText.setVisibility(GONE);
            callButton.setVisibility(GONE);
        }
    }

    private void setPhoneClickListener(@NonNull String phoneNumber) {
        final View.OnClickListener onClickListener = view ->
                IntentUtils.startDialer(getContext(), phoneNumber);
        phoneText.setOnClickListener(onClickListener);
        callButton.setOnClickListener(onClickListener);
    }

    private void setAddressText(@Nullable CharSequence address) {
        if (!TextUtils.isEmpty(address)) {
            addressText.setVisibility(VISIBLE);
            addressText.setText(place.getAddress());
            setAddressClickListener(address.toString());
        } else {
            addressText.setVisibility(GONE);
        }
    }

    private void setAddressClickListener(@NonNull String address) {
        addressText.setOnClickListener(view -> IntentUtils.startShareIntent(getContext(), address));
    }

    private void setWebsiteText(@Nullable Uri websiteUri) {
        if (websiteUri != null) {
            websiteText.setVisibility(VISIBLE);
            websiteButton.setVisibility(VISIBLE);
            websiteText.setText(websiteUri.toString());
            setWebsiteClickListener(websiteUri);
        } else {
            websiteText.setVisibility(GONE);
            websiteButton.setVisibility(GONE);
        }
    }

    private void setWebsiteClickListener(@NonNull Uri websiteUri) {
        final View.OnClickListener onClickListener = view ->
                IntentUtils.startCustomTabs(getContext(), websiteUri);
        websiteText.setOnClickListener(onClickListener);
        websiteButton.setOnClickListener(onClickListener);
    }

    private void setWeatherRecyclerView() {
        final PlacesWeatherRecyclerAdapter adapter = new PlacesWeatherRecyclerAdapter(locationItem);
        weatherRecyclerView.setAdapter(adapter);
    }

    private void setAlphaToMinimizedViews(final float alpha) {
        locationNameText.setAlpha(alpha);
        tempTextPlaceholder.setAlpha(alpha);
        tempText.setAlpha(alpha);
        conditionTextPlaceholder.setAlpha(alpha);
        conditionText.setAlpha(alpha);
        actionButtonPlaceholder.setAlpha(alpha);
        actionButton.setAlpha(alpha);
        moreInfoText.setAlpha(alpha);
    }

    private void setAlphaToMaximizedViews(final float alpha) {
        maximizedLayoutPlaceholder.setAlpha(alpha);
        maximizedLayout.setAlpha(alpha);
    }

    private void setMaximizedViewsGone() {
        maximizedLayoutPlaceholder.setVisibility(GONE);
        maximizedLayout.setVisibility(GONE);
    }

    private void setMinimizedViewsGone() {
        saveCurrentVisibilities();
        locationNameText.setVisibility(GONE);
        tempTextPlaceholder.setVisibility(GONE);
        tempText.setVisibility(GONE);
        conditionTextPlaceholder.setVisibility(GONE);
        conditionText.setVisibility(GONE);
        actionButtonPlaceholder.setVisibility(GONE);
        actionButton.setVisibility(GONE);
        moreInfoText.setVisibility(GONE);
    }

    private void saveCurrentVisibilities() {
        actionButtonPlaceholderVisibility = actionButtonPlaceholder.getVisibility();
        actionButtonVisibility = actionButton.getVisibility();
        conditionTextPlaceholderVisibility = conditionTextPlaceholder.getVisibility();
        conditionTextVisibility = conditionText.getVisibility();
        tempTextPlaceholderVisibility = tempTextPlaceholder.getVisibility();
        tempTextVisibility = tempText.getVisibility();
        this.instanceSaved = true;
    }

    private void restoreVisibilities() {
        if (instanceSaved) {
            locationNameText.setVisibility(VISIBLE);
            tempTextPlaceholder.setVisibility(tempTextPlaceholderVisibility);
            tempText.setVisibility(tempTextVisibility);
            conditionTextPlaceholder.setVisibility(conditionTextPlaceholderVisibility);
            conditionText.setVisibility(conditionTextVisibility);
            actionButtonPlaceholder.setVisibility(actionButtonPlaceholderVisibility);
            actionButton.setVisibility(actionButtonVisibility);
            moreInfoText.setVisibility(VISIBLE);
        } else {
            instanceSaved = false;
        }
    }

    private void hideIfNotInflated() {
        if (TextUtils.isEmpty(locationNameText.getText())) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            setMinimizedLayoutViewsInvisible();
        }
    }

    public void setLocation(@NonNull OptimizedLocationItem locationItem) {
        this.locationItem = locationItem;
        this.existsInDb = locationItem.getLocation().isAdded();
        inflateLayout();
    }

    private void inflateLayout() {
        setMinimizedLayoutViewsVisible();
        final OptimizedConditionItem item = locationItem.getCondition();
        tempText.setText(getContext().getString(R.string.degree, item.getCurrentTemp()) + locationItem.getTempUnit());
        conditionText.setText(item.getConditionId());
        configActionButton();
    }

    private void setMinimizedLayoutViewsVisible() {
        setViewVisible(tempTextPlaceholder, tempText);
        setViewVisible(conditionTextPlaceholder, conditionText);
        setViewVisible(actionButtonPlaceholder, actionButton);
    }

    private void setViewVisible(@NonNull ProgressBar placeHolder, @NonNull View view) {
        placeHolder.setVisibility(INVISIBLE);
        view.setVisibility(VISIBLE);
    }

    private void setMinimizedLayoutViewsInvisible() {
        setViewInvisible(tempTextPlaceholder, tempText);
        setViewInvisible(conditionTextPlaceholder, conditionText);
        setViewInvisible(actionButtonPlaceholder, actionButton);
    }

    private void setViewInvisible(@NonNull ProgressBar placeHolder, @NonNull View view) {
        view.setVisibility(INVISIBLE);
        placeHolder.setVisibility(VISIBLE);
    }

    private void configActionButton() {
        if (existsInDb) {
            showDeleteActionButton();
            configDeleteActionButtonClick();
        } else {
            showAddActionButton();
            configAddActionButtonClick();
        }
    }

    private void showAddActionButton() {
        actionButton.setBackgroundResource(R.drawable.shape_button_material_green);
        actionButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_white_24dp, 0, 0, 0);
        actionButton.setText(R.string.add_location);
        addButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_add_green_24dp, 0, 0);
        addButton.setText(R.string.add);
    }

    private void showDeleteActionButton() {
        actionButton.setBackgroundResource(R.drawable.shape_button_material_red);
        actionButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_delete_white_24dp, 0, 0, 0);
        actionButton.setText(R.string.action_delete_location);
        addButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_delete_green_24dp, 0, 0);
        addButton.setText(R.string.delete);
    }

    private void configDeleteActionButtonClick() {
        final View.OnClickListener onClickListener = view -> {
            callback.onDeletePressed(locationItem);
            existsInDb = false;
            showAddActionButton();
            configAddActionButtonClick();
        };
        actionButton.setOnClickListener(onClickListener);
        addButton.setOnClickListener(onClickListener);
    }

    private void configAddActionButtonClick() {
        final View.OnClickListener onClickListener = view -> {
            callback.onAddPressed(locationItem);
            existsInDb = true;
            showDeleteActionButton();
            configDeleteActionButtonClick();
        };
        actionButton.setOnClickListener(onClickListener);
        addButton.setOnClickListener(onClickListener);
    }

    public void notifyNoWeatherDataAvailable() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            hide();
            FancyToast.makeText(getContext(), getResources().getString(R.string.error_no_weather_data),
                    FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }

    private void prepareMaximizedLayout() {
        geoDataClient.getPlaceById(locationItem.getLocation().getId())
                .addOnSuccessListener(this::doOnPlaceIdSuccess)
                .addOnFailureListener(e -> {
                    doOnPlaceIdError();
                    Crashlytics.logException(e);
                });
    }

    private void doOnPlaceIdSuccess(@NonNull PlaceBufferResponse places) {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN) {
            if (places.getCount() > 0) {
                inflatePlace(places);
            } else {
                doOnPlaceIdError();
            }
        }
        places.release();
    }

    private void inflatePlace(@NonNull PlaceBufferResponse places) {
        place = places.get(0).freeze();
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            inflateMaximizedLayout();
        }
    }

    private void doOnPlaceIdError() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_HIDDEN && !swipeUp) {
            FancyToast.makeText(getContext(), getContext().getString(R.string.error_geodata_fail),
                    FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public void collapse() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public void submitList(@Nullable PagedList<LocationPhoto> photos) {
        adapter.submitList(photos);
        if (photos == null || photos.size() == 0) {
            photosRecyclerView.setVisibility(GONE);
        } else {
            photosRecyclerView.setVisibility(VISIBLE);
        }
    }

    public interface Callback {
        void onDeletePressed(@NonNull OptimizedLocationItem item);

        void onAddPressed(@NonNull OptimizedLocationItem item);

        void onBottomSheetExpanded(@NonNull String locationName);

        void onBottomSheetCollapsed();

        void onBottomSheetPreparingMaximizedLayout(@NonNull String id);
    }
}
