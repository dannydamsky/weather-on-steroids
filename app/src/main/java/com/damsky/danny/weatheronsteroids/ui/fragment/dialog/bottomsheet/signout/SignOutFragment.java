package com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.signout;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.GlideApp;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.auth.AuthHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This fragment contains a BottomSheetDialog. Its purpose is to allow the user to sign out of their
 * Google account if they wish to do so.
 * When using this fragment you must implement {@link Listener}.
 *
 * @author Danny Damsky
 */
public final class SignOutFragment extends BottomSheetDialogFragment {

    /**
     * Shows the dialog.
     *
     * @param fragmentManager the fragment manager that should hold the dialog.
     */
    public static void show(@NonNull FragmentManager fragmentManager) {
        final SignOutFragment fragment = new SignOutFragment();
        fragment.show(fragmentManager, TAG);
    }

    private static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.signout.SignOutFragment.TAG";

    @BindView(R.id.userImage)
    ImageView userImage;
    @BindView(R.id.userName)
    TextView userName;

    private Listener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (Listener) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(getContext())
                .inflate(R.layout.fragment_sign_out, container, false);
        ButterKnife.bind(this, layout);
        bindViews();
        return layout;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog d = super.onCreateDialog(savedInstanceState);
        d.setOnShowListener(dialogInterface -> {
            final BottomSheetDialog dialog = (BottomSheetDialog) dialogInterface;
            final FrameLayout bottomSheet = dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        return d;
    }

    private void bindViews() {
        userName.setText(AuthHelper.getDisplayName());
        GlideApp.with(this).load(AuthHelper.getPhotoUrl()).circleCrop().into(userImage);
    }

    @OnClick(R.id.positiveButton)
    public void onPositiveButtonClicked() {
        listener.onLogOutPressed();
        dismiss();
    }

    @OnClick(R.id.negativeButton)
    public void onNegativeButtonClicked() {
        dismiss();
    }

    public interface Listener {
        void onLogOutPressed();
    }
}
