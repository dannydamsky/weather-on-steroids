package com.damsky.danny.weatheronsteroids.data.maps;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * This class is used by the ClusterManager class for adding markers to the map in MapFragment.
 *
 * @author Danny Damsky
 * @see com.google.maps.android.clustering.ClusterManager
 * @see com.damsky.danny.weatheronsteroids.ui.fragment.mainmap.MapFragment
 */
public final class MapClusterItem implements ClusterItem {

    private final LatLng position;
    private final String title;
    private final String snippet;

    public MapClusterItem(@NonNull OptimizedLocationItem item) {
        final OptimizedWeatherLocation location = item.getLocation();
        final double lat = location.getLatitude();
        final double lng = location.getLongitude();
        this.position = new LatLng(lat, lng);
        this.title = location.getLocationName();
        this.snippet = item.getCondition().getCurrentTemp() + "°" + item.getTempUnit();
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }

    @Override
    public String toString() {
        return "MapClusterItem{" +
                "position=" + position +
                ", title='" + title + '\'' +
                ", snippet='" + snippet + '\'' +
                '}';
    }
}
