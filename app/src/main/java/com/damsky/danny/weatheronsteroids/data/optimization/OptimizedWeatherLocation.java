package com.damsky.danny.weatheronsteroids.data.optimization;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;

import java.util.Objects;

/**
 * This class contains the same fields as {@link WeatherLocation} except for the
 * YAHOO Weather response JSON, which gets parsed in {@link OptimizedLocationItem}.
 *
 * @author Danny Damsky
 * @see OptimizedLocationItem
 * @see WeatherLocation
 */

public final class OptimizedWeatherLocation {

    private final String id;
    private final String locationName;
    private final double latitude;
    private final double longitude;
    private final long lastUpdated;
    private final boolean isAdded;

    OptimizedWeatherLocation(@NonNull WeatherLocation location) {
        this.id = location.getId();
        this.locationName = location.getLocationName();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.lastUpdated = location.getLastUpdated();
        this.isAdded = location.isAdded();
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getLocationName() {
        return locationName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public boolean isAdded() {
        return isAdded;
    }

    @Override
    public String toString() {
        return "OptimizedWeatherLocation{" +
                "id='" + id + '\'' +
                ", locationName='" + locationName + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", lastUpdated=" + lastUpdated +
                ", isAdded=" + isAdded +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptimizedWeatherLocation)) return false;
        final OptimizedWeatherLocation location = (OptimizedWeatherLocation) o;
        return Double.compare(location.latitude, latitude) == 0 &&
                Double.compare(location.longitude, longitude) == 0 &&
                lastUpdated == location.lastUpdated &&
                isAdded == location.isAdded &&
                Objects.equals(id, location.id) &&
                Objects.equals(locationName, location.locationName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, locationName, latitude, longitude, lastUpdated, isAdded);
    }
}
