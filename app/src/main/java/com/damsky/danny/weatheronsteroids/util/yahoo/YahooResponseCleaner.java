package com.damsky.danny.weatheronsteroids.util.yahoo;

import android.support.annotation.NonNull;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is used to remove unused objects from the YAHOO Weather response JSON.
 *
 * @author Danny Damsky
 */
public final class YahooResponseCleaner {

    private static final String OBJECT_QUERY = "query";
    private static final String QUERY_COUNT = "count";
    private static final String QUERY_RESULTS = "results";
    private static final String RESULTS_CHANNEL = "channel";
    private static final String CHANNEL_TITLE = "title";
    private static final String CHANNEL_LINK = "link";
    private static final String CHANNEL_DESCRIPTION = "description";
    private static final String CHANNEL_LANGUAGE = "language";
    private static final String CHANNEL_LAST_BUILD_DATE = "lastBuildDate";
    private static final String CHANNEL_TTL = "ttl";
    private static final String CHANNEL_LOCATION = "location";
    private static final String CHANNEL_IMAGE = "image";
    private static final String CHANNEL_UNITS = "units";
    private static final String CHANNEL_ATMOSPHERE = "atmosphere";
    private static final String CHANNEL_ITEM = "item";
    private static final String UNITS_DISTANCE = "distance";
    private static final String UNITS_PRESSURE = "pressure";
    private static final String UNITS_SPEED = "speed";
    private static final String ATMOSPHERE_RISING = "rising";
    private static final String ITEM_TITLE = "title";
    private static final String ITEM_LINK = "link";
    private static final String ITEM_PUB_DATE = "pubDate";
    private static final String ITEM_DESCRIPTION = "description";
    private static final String ITEM_GUID = "guid";
    private static final String ITEM_CONDITION = "condition";
    private static final String ITEM_FORECAST = "forecast";
    private static final String CONDITION_DATE = "date";
    private static final String CONDITION_TEXT = "text";
    private static final String FORECAST_DATE = "date";
    private static final String FORECAST_TEXT = "text";

    public static String clean(@NonNull String json) {
        try {
            return getTrimmedJson(json);
        } catch (JSONException e) {
            Crashlytics.logException(e);
        }
        return json;
    }

    private static String getTrimmedJson(@NonNull String json) throws JSONException {
        final JSONObject query = new JSONObject(json).getJSONObject(OBJECT_QUERY);
        final int count = query.getInt(QUERY_COUNT);
        final JSONObject results = query.getJSONObject(QUERY_RESULTS);
        decideResultParsingStrategy(count, results);
        return results.toString();
    }

    private static void decideResultParsingStrategy(int count, JSONObject results) throws JSONException {
        if (count == 1) {
            trimResultsOverSingleChannel(results);
        } else {
            trimResultsOverMultipleChannels(results);
        }
    }

    private static void trimResultsOverSingleChannel(JSONObject results) throws JSONException {
        final JSONObject channel = results.getJSONObject(RESULTS_CHANNEL);
        trimResults(channel);
    }

    private static void trimResultsOverMultipleChannels(JSONObject results) throws JSONException {
        final JSONArray channel = results.getJSONArray(RESULTS_CHANNEL);
        final int length = channel.length();
        for (int i = 0; i < length; i++) {
            trimResults(channel.getJSONObject(i));
        }
    }

    private static void trimResults(JSONObject channel) throws JSONException {
        trimChannelProperties(channel);
        trimUnitsProperties(channel);
        trimAtmosphereProperties(channel);
        processChannelItem(channel);
    }

    private static void trimChannelProperties(JSONObject channel) {
        channel.remove(CHANNEL_TITLE);
        channel.remove(CHANNEL_LINK);
        channel.remove(CHANNEL_DESCRIPTION);
        channel.remove(CHANNEL_LANGUAGE);
        channel.remove(CHANNEL_LAST_BUILD_DATE);
        channel.remove(CHANNEL_TTL);
        channel.remove(CHANNEL_LOCATION);
        channel.remove(CHANNEL_IMAGE);
    }

    private static void trimUnitsProperties(JSONObject channel) throws JSONException {
        final JSONObject units = channel.getJSONObject(CHANNEL_UNITS);
        units.remove(UNITS_DISTANCE);
        units.remove(UNITS_PRESSURE);
        units.remove(UNITS_SPEED);
    }

    private static void trimAtmosphereProperties(JSONObject channel) throws JSONException {
        final JSONObject atmosphere = channel.getJSONObject(CHANNEL_ATMOSPHERE);
        atmosphere.remove(ATMOSPHERE_RISING);
    }

    private static void processChannelItem(JSONObject channel) throws JSONException {
        final JSONObject item = channel.getJSONObject(CHANNEL_ITEM);
        trimItemProperties(item);
        trimConditionProperties(item);
        iterateOverForecasts(item);
    }

    private static void trimItemProperties(JSONObject item) {
        item.remove(ITEM_TITLE);
        item.remove(ITEM_LINK);
        item.remove(ITEM_PUB_DATE);
        item.remove(ITEM_DESCRIPTION);
        item.remove(ITEM_GUID);
    }

    private static void trimConditionProperties(JSONObject item) throws JSONException {
        final JSONObject condition = item.getJSONObject(ITEM_CONDITION);
        condition.remove(CONDITION_DATE);
        condition.remove(CONDITION_TEXT);
    }

    private static void iterateOverForecasts(JSONObject item) throws JSONException {
        final JSONArray forecast = item.getJSONArray(ITEM_FORECAST);
        final int length = forecast.length();
        for (int i = 0; i < length; i++) {
            trimForecastProperties(forecast.getJSONObject(i));
        }
    }

    private static void trimForecastProperties(JSONObject forecastItem) {
        forecastItem.remove(FORECAST_DATE);
        forecastItem.remove(FORECAST_TEXT);
    }

    private YahooResponseCleaner() {
    }
}
