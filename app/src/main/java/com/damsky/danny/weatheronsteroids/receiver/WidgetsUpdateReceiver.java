package com.damsky.danny.weatheronsteroids.receiver;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.damsky.danny.weatheronsteroids.widget.ClockWeatherWidget;

public final class WidgetsUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null) {
            switch (action) {
                case Intent.ACTION_TIME_TICK:
                case Intent.ACTION_BOOT_COMPLETED:
                case Intent.ACTION_DATE_CHANGED:
                case Intent.ACTION_LOCALE_CHANGED:
                case Intent.ACTION_SCREEN_ON:
                case Intent.ACTION_TIME_CHANGED:
                case Intent.ACTION_TIMEZONE_CHANGED:
                    sendBroadcast(context.getApplicationContext());
                    break;
            }
        }
    }

    private void sendBroadcast(Context context) {
        final Intent updateWidgetIntent = new Intent(context, ClockWeatherWidget.class);
        updateWidgetIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        context.sendBroadcast(updateWidgetIntent);
    }
}
