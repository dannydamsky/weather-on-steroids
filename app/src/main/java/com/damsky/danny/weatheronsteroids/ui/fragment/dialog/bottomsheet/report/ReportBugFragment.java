package com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.report;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.util.KeyboardUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * This fragment contains a BottomSheetDialog with an EditText and a report button.
 * Its purpose is to send bug reports to the author.
 *
 * @author Danny Damsky
 */
public final class ReportBugFragment extends BottomSheetDialogFragment {

    /**
     * Shows the dialog.
     *
     * @param fragmentManager the fragment manager that should hold the dialog.
     */
    public static void show(@NonNull FragmentManager fragmentManager) {
        final ReportBugFragment fragment = new ReportBugFragment();
        fragment.show(fragmentManager, TAG);
    }

    private static final String TAG =
            "com.damsky.danny.weatheronsteroids.ui.fragment.dialog.bottomsheet.report.ReportBugFragment.TAG";

    @BindView(R.id.editableBugReport)
    EditText editableBugReport;

    private MainActivity activity;
    private Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog d = super.onCreateDialog(savedInstanceState);
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        d.setOnShowListener(dialogInterface -> {
            final BottomSheetDialog dialog = (BottomSheetDialog) dialogInterface;
            final FrameLayout bottomSheet = dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        return d;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_report_bug, container, false);
        unbinder = ButterKnife.bind(this, layout);
        KeyboardUtils.setBelowDialog(activity, layout);
        return layout;
    }

    @OnClick(R.id.reportButton)
    public void onReportButtonClicked() {
        final String text = editableBugReport.getText().toString().trim();
        if (!text.isEmpty()) {
            sendEmail(text);
        }
        dismiss();
    }

    private void sendEmail(@NonNull String text) {
        Crashlytics.log(Log.ERROR, "USER_REPORT", text);
        final String mailto = Constants.URL_EMAIL +
                "?subject=" + Uri.encode("Weather On Steroids Bug Report") +
                "&body=" + text;
        final Intent mailIntent = new Intent(Intent.ACTION_SENDTO);
        mailIntent.setData(Uri.parse(mailto));
        startActivity(mailIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
