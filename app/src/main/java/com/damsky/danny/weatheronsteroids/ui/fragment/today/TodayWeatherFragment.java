package com.damsky.danny.weatheronsteroids.ui.fragment.today;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationViewModel;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedForecastItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.ui.activity.MainActivity;
import com.damsky.danny.weatheronsteroids.ui.widget.ArrowView;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;
import com.damsky.danny.weatheronsteroids.work.DbOpsWorker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This class displays the "today" weather of a location in great detail.
 * It's contained inside {@link com.damsky.danny.weatheronsteroids.ui.fragment.pager.weatherpager.WeatherViewPagerFragment}.
 *
 * @author Danny Damsky
 */
public final class TodayWeatherFragment extends Fragment {

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.dateTextView)
    TextView dateTextView;
    @BindView(R.id.dayNightTempsText)
    TextView dayNightTempsText;
    @BindView(R.id.tempText)
    TextView tempText;
    @BindView(R.id.tempImage)
    ImageView tempImage;
    @BindView(R.id.feelsLikeTempText)
    TextView feelsLikeTempText;
    @BindView(R.id.weatherConditionText)
    TextView weatherConditionText;
    @BindView(R.id.humidityText)
    TextView humidityText;
    @BindView(R.id.pressureText)
    TextView pressureText;
    @BindView(R.id.visibilityText)
    TextView visibilityText;
    @BindView(R.id.chillText)
    TextView chillText;
    @BindView(R.id.directionArrow)
    ArrowView directionArrow;
    @BindView(R.id.speedText)
    TextView speedText;
    @BindView(R.id.sunriseText)
    TextView sunriseText;
    @BindView(R.id.sunsetText)
    TextView sunsetText;
    @BindView(R.id.yahooAdCard)
    CardView yahooAdCard;

    private MainActivity activity;
    private Unbinder unbinder;
    private OptimizedLocationItem locationItem;
    private WeatherLocationViewModel weatherLocationViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherLocationViewModel = ViewModelProviders.of(this).get(WeatherLocationViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_today, container, false);
        unbinder = ButterKnife.bind(this, layout);
        configureRefreshLayoutColorScheme();
        setObserver(getArguments().getString(Constants.EXTRA_ID));
        return layout;
    }

    private void configureRefreshLayoutColorScheme() {
        refreshLayout.setColorSchemeResources(R.color.colorSecondary, R.color.colorTertiary,
                R.color.colorSecondaryLight, R.color.colorTertiaryLight, R.color.colorSecondaryDark,
                R.color.colorTertiaryDark);
    }

    private void setObserver(String itemId) {
        final LiveData<OptimizedLocationItem> liveData = weatherLocationViewModel.getOptimizedById(itemId);
        liveData.observe(this, locationItem -> {
            this.locationItem = locationItem;
            setResults();
        });
    }

    private void setResults() {
        if (unbinder != null) {
            if (this.refreshLayout.isRefreshing()) {
                this.refreshLayout.setRefreshing(false);
            }
            configureViews();
        }
    }

    private void configureViews() {
        configureMeasurelessViews();
        if (locationItem.isMetric()) {
            configureViewsForMetricSystem();
        } else {
            configureViewsForImperialSystem();
        }
    }

    private void configureMeasurelessViews() {
        setRefreshLayout();
        setDateTextView();
        setDayNightTempsText();
        setTempImage();
        setFeelsLikeTempText();
        setWeatherConditionText();
        setHumidityText();
        setDirectionArrow();
        setYahooAdCard();
        setSunsetText();
        setSunriseText();
        setChillText();
    }

    private void configureViewsForMetricSystem() {
        setTempTextMetric();
        setPressureTextMetric();
        setVisibilityTextMetric();
        setSpeedTextMetric();
    }

    private void configureViewsForImperialSystem() {
        setTempTextImperial();
        setPressureTextImperial();
        setVisibilityTextImperial();
        setSpeedTextImperial();
    }

    private void setRefreshLayout() {
        refreshLayout.setOnRefreshListener(() ->
                DbOpsWorker.updateSinglePlace(activity, locationItem.getLocation()));
    }

    private void setDateTextView() {
        final long timeInMillis = locationItem.getLocation().getLastUpdated();
        final String date = DateUtils.formatDateTime(activity, timeInMillis, DateUtils.FORMAT_SHOW_DATE);
        final String time = DateUtils.formatDateTime(activity, timeInMillis, DateUtils.FORMAT_SHOW_TIME);
        dateTextView.setText(String.format("%s, %s", date, time));
    }

    private void setDayNightTempsText() {
        final OptimizedForecastItem forecast = locationItem.getForecast()[0];
        dayNightTempsText.setText(getString(R.string.day_night_temps, forecast.getDayTemp(), forecast.getNightTemp()));
    }

    private void setTempTextMetric() {
        final int temp = locationItem.getCondition().getCurrentTemp();
        tempText.setText(getString(R.string.celsius, temp));
    }

    private void setTempTextImperial() {
        final int temp = locationItem.getCondition().getCurrentTemp();
        tempText.setText(getString(R.string.fahrenheit, temp));
    }

    private void setTempImage() {
        tempImage.setImageResource(locationItem.getCondition().getConditionImageId());
    }

    private void setFeelsLikeTempText() {
        feelsLikeTempText.setText(String.format("%s%s",
                getString(R.string.feels_like, locationItem.getAtmosphere().getRealFeel()),
                locationItem.getTempUnit()));
    }

    private void setWeatherConditionText() {
        weatherConditionText.setText(locationItem.getCondition().getConditionId());
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setHumidityText() {
        humidityText.setText(locationItem.getAtmosphere().getHumidity() + "%");
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setPressureTextMetric() {
        pressureText.setText(locationItem.getAtmosphere().getPressure() + " " + getString(R.string.mbar));
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setPressureTextImperial() {
        pressureText.setText(locationItem.getAtmosphere().getPressure() + " " + getString(R.string.in));
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setVisibilityTextMetric() {
        final float visibility = locationItem.getAtmosphere().getVisibility();
        visibilityText.setText(visibility + " " + getString(R.string.km));
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setVisibilityTextImperial() {
        final float visibility = locationItem.getAtmosphere().getVisibility();
        visibilityText.setText(visibility + " " + getString(R.string.mi));
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setChillText() {
        final int temp = locationItem.getWind().getChillTemp();
        chillText.setText(getString(R.string.degree, temp) + locationItem.getTempUnit());
    }

    private void setDirectionArrow() {
        final int degrees = locationItem.getWind().getDirectionDegrees();
        directionArrow.setDegrees(degrees);
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setSpeedTextMetric() {
        final double speed = locationItem.getWind().getSpeed();
        speedText.setText(speed + " " + getString(R.string.kmh));
    }

    /**
     * String concatenation works just fine here with all languages (LTR and RTL)
     */
    @SuppressLint("SetTextI18n")
    private void setSpeedTextImperial() {
        final double speed = locationItem.getWind().getSpeed();
        speedText.setText(speed + " " + getString(R.string.mph));
    }

    private void setSunriseText() {
        final String sunriseTime = locationItem.getAstronomy().getSunriseTime();
        sunriseText.setText(sunriseTime);
    }

    private void setSunsetText() {
        final String sunsetTime = locationItem.getAstronomy().getSunsetTime();
        sunsetText.setText(sunsetTime);
    }

    private void setYahooAdCard() {
        yahooAdCard.setOnClickListener(v ->
                IntentUtils.startCustomTabs(activity, Constants.URL_YAHOO));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
