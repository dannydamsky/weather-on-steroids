package com.damsky.danny.weatheronsteroids.widget;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.widget.RemoteViews;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.R;
import com.damsky.danny.weatheronsteroids.data.db.WeatherLocationRepository;
import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedConditionItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedForecastItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedLocationItem;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.receiver.WidgetsUpdateReceiver;
import com.damsky.danny.weatheronsteroids.util.IntentUtils;
import com.damsky.danny.weatheronsteroids.util.Time;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link ClockWeatherWidgetConfigureActivity ClockWeatherWidgetConfigureActivity}
 */
public final class ClockWeatherWidget extends AppWidgetProvider {

    @SuppressLint("CheckResult")
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        final String locationId = PreferencesHelper.getInstance().getWidgetLocationId(appWidgetId);
        if (locationId != null) {
            Observable.just(locationId)
                    .map(s -> {
                        final WeatherLocation weatherLocation = WeatherLocationRepository
                                .getInstance(context).getByIdBlocking(s);
                        return new OptimizedLocationItem(weatherLocation);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(optimizedLocationItem -> {
                        try {
                            consumeResultAndUpdateAppWidget(context, appWidgetManager, appWidgetId,
                                    optimizedLocationItem);
                        } catch (Exception e) {
                            Crashlytics.logException(e);
                        }
                    });
        }
    }

    private static void consumeResultAndUpdateAppWidget(Context context,
                                                        AppWidgetManager appWidgetManager,
                                                        int appWidgetId,
                                                        OptimizedLocationItem optimizedLocationItem) {
        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.clock_weather_widget);
        updateTimeSection(views);
        updateWeatherSection(context, views, optimizedLocationItem);
        setOnClickListeners(context, views);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private static void updateTimeSection(final RemoteViews views) {
        final Time time = Time.getInstance();
        views.setTextViewText(R.id.amPmText, time.getAmPm());
        views.setTextViewText(R.id.hoursText, time.getHours());
        views.setTextViewText(R.id.minutesText, time.getMinutes());
    }

    @SuppressLint("CheckResult")
    private static void updateWeatherSection(Context context, final RemoteViews views,
                                             OptimizedLocationItem optimizedLocationItem) {
        final String formattedTime = getFormattedTime(context);
        final OptimizedWeatherLocation optimizedWeatherLocation = optimizedLocationItem.getLocation();
        final OptimizedConditionItem optimizedConditionItem = optimizedLocationItem.getCondition();
        final OptimizedForecastItem optimizedForecastItem = optimizedLocationItem.getForecast()[0];
        final Resources resources = context.getResources();
        final int unitsResourceId = optimizedLocationItem.isMetric() ? R.string.celsius : R.string.fahrenheit;

        views.setTextViewText(R.id.dateText, formattedTime);
        views.setTextViewText(R.id.locationText, optimizedWeatherLocation.getLocationName());
        views.setTextViewText(R.id.conditionText, resources.getString(optimizedConditionItem.getConditionId()));
        views.setTextViewText(R.id.currentTempText, resources.getString(unitsResourceId, optimizedConditionItem.getCurrentTemp()));
        views.setTextViewText(R.id.dayNightTempsText,
                resources.getString(unitsResourceId, optimizedForecastItem.getDayTemp())
                        + " / " + resources.getString(unitsResourceId, optimizedForecastItem.getNightTemp()));
    }

    @NonNull
    private static String getFormattedTime(Context context) {
        final long currentTimeInMillis = System.currentTimeMillis();
        final String weekDay = DateUtils.formatDateTime(context, currentTimeInMillis,
                DateUtils.FORMAT_SHOW_WEEKDAY);
        final String date = DateUtils.formatDateTime(context,
                currentTimeInMillis, DateUtils.FORMAT_SHOW_DATE);
        return String.format("%s, %s", weekDay, date);
    }

    private static void setOnClickListeners(Context context, final RemoteViews views) {
        views.setOnClickPendingIntent(R.id.timeSection, IntentUtils.getPendingIntentForClockApp(context));
        views.setOnClickPendingIntent(R.id.weatherSection, IntentUtils.getPendingIntentForMainActivity(context));
    }

    private final WidgetsUpdateReceiver receiver = new WidgetsUpdateReceiver();

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        registerReceiver(context);
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        if (appWidgetManager != null) {
            findProvidersAndUpdateWidget(context, intent, appWidgetManager);
        }
    }

    private void findProvidersAndUpdateWidget(Context context, Intent intent, AppWidgetManager appWidgetManager) {
        final ComponentName provider = new ComponentName(context, getClass());
        final int[] appWidgetIds = appWidgetManager.getAppWidgetIds(provider);
        final String action = intent.getAction();
        if (action != null && action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            runUpdateOnAllWidgets(context, appWidgetManager, appWidgetIds);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        registerReceiver(context);
        runUpdateOnAllWidgets(context, appWidgetManager, appWidgetIds);
    }

    private void runUpdateOnAllWidgets(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        PreferencesHelper.getInstance().removeWidgetInfo(appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        registerReceiver(context);
    }

    @Override
    public void onDisabled(Context context) {
        unregisterReceiver(context);
    }

    private void registerReceiver(Context context) {
        try {
            context.getApplicationContext().registerReceiver(receiver, getIntentFilter());
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_BOOT_COMPLETED);
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        intentFilter.addAction(Intent.ACTION_LOCALE_CHANGED);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        return intentFilter;
    }

    private void unregisterReceiver(Context context) {
        try {
            context.getApplicationContext().unregisterReceiver(receiver);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }
}

