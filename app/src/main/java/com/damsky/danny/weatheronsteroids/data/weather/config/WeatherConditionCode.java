package com.damsky.danny.weatheronsteroids.data.weather.config;

import android.support.annotation.DrawableRes;
import android.support.annotation.IntRange;
import android.support.annotation.StringRes;

import com.damsky.danny.weatheronsteroids.R;

/**
 * This Enum class helps to keep track of YAHOO Weather API condition codes.
 * Each enum contains 3 parameters: the first is the condition code itself, the second is a reference
 * to the string resource for the condition, and the third is a reference to the drawable resource
 * for the condition.
 *
 * @author Danny Damsky
 * @see <a href="https://developer.yahoo.com/weather/documentation.html?guccounter=1#codes">
 * Condition codes
 * </a>
 */
public enum WeatherConditionCode {
    TORNADO(0, R.string.condition_tornado, R.drawable.ic_weather_hurricane_primary_black_24dp),
    TROPICAL_STORM(1, R.string.condition_tropical_storm, R.drawable.ic_weather_lightning_primary_black_24dp),
    HURRICANE(2, R.string.condition_hurricane, R.drawable.ic_weather_hurricane_primary_black_24dp),
    SEVERE_THUNDERSTORMS(3, R.string.condition_severe_thunderstorms, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    THUNDERSTORMS(4, R.string.condition_thunderstorms, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    MIXED_RAIN_AND_SNOW(5, R.string.condition_mixed_rain_and_snow, R.drawable.ic_weather_snowy_rainy_primary_black_24dp),
    MIXED_RAIN_AND_SLEET(6, R.string.condition_mixed_rain_and_sleet, R.drawable.ic_weather_rainy_primary_black_24dp),
    MIXED_SNOW_AND_SLEET(7, R.string.condition_mixed_snow_and_sleet, R.drawable.ic_weather_snowy_rainy_primary_black_24dp),
    FREEZING_DRIZZLE(8, R.string.condition_freezing_drizzle, R.drawable.ic_weather_rainy_primary_black_24dp),
    DRIZZLE(9, R.string.condition_drizzle, R.drawable.ic_weather_rainy_primary_black_24dp),
    FREEZING_RAIN(10, R.string.condition_freezing_rain, R.drawable.ic_weather_rainy_primary_black_24dp),
    SHOWERS(11, R.string.condition_showers, R.drawable.ic_weather_rainy_primary_black_24dp),
    SHOWERS_2(12, R.string.condition_showers, R.drawable.ic_weather_rainy_primary_black_24dp),
    SNOW_FLURRIES(13, R.string.condition_snow_flurries, R.drawable.ic_weather_snowy_primary_black_24dp),
    LIGHT_SNOW_SHOWERS(14, R.string.condition_light_snow_showers, R.drawable.ic_weather_snowy_primary_black_24dp),
    BLOWING_SNOW(15, R.string.condition_blowing_snow, R.drawable.ic_weather_snowy_primary_black_24dp),
    SNOW(16, R.string.condition_snow, R.drawable.ic_weather_snowy_primary_black_24dp),
    HAIL(17, R.string.condition_hail, R.drawable.ic_weather_hail_primary_black_24dp),
    SLEET(18, R.string.condition_sleet, R.drawable.ic_weather_rainy_primary_black_24dp),
    DUST(19, R.string.condition_dust, R.drawable.ic_weather_fog_primary_black_24dp),
    FOGGY(20, R.string.condition_foggy, R.drawable.ic_weather_fog_primary_black_24dp),
    HAZE(21, R.string.condition_haze, R.drawable.ic_weather_fog_primary_black_24dp),
    SMOKY(22, R.string.condition_smoky, R.drawable.ic_weather_fog_primary_black_24dp),
    BLUSTERY(23, R.string.condition_blustery, R.drawable.ic_weather_windy_primary_black_24dp),
    WINDY(24, R.string.condition_windy, R.drawable.ic_weather_windy_primary_black_24dp),
    COLD(25, R.string.condition_cold, R.drawable.ic_ac_unit_primary_black_24dp),
    CLOUDY(26, R.string.condition_cloudy, R.drawable.ic_weather_cloudy_primary_black_24dp),
    MOSTLY_CLOUDY_NIGHT(27, R.string.condition_mostly_cloudy, R.drawable.ic_weather_cloudy_primary_black_24dp),
    MOSTLY_CLOUDY_DAY(28, R.string.condition_mostly_cloudy, R.drawable.ic_weather_cloudy_primary_black_24dp),
    PARTLY_CLOUDY_NIGHT(29, R.string.condition_partly_cloudy, R.drawable.ic_weather_partlycloudy_primary_black_24dp),
    PARTLY_CLOUDY_DAY(30, R.string.condition_partly_cloudy, R.drawable.ic_weather_partlycloudy_primary_black_24dp),
    CLEAR_NIGHT(31, R.string.condition_clear, R.drawable.ic_weather_night_primary_black_24dp),
    SUNNY(32, R.string.condition_sunny, R.drawable.ic_weather_sunny_primary_black_24dp),
    FAIR_NIGHT(33, R.string.condition_fair, R.drawable.ic_weather_night_primary_black_24dp),
    FAIR_DAY(34, R.string.condition_fair, R.drawable.ic_weather_sunny_primary_black_24dp),
    MIXED_RAIN_AND_HAIL(35, R.string.condition_mixed_rain_and_hail, R.drawable.ic_weather_hail_primary_black_24dp),
    HOT(36, R.string.condition_hot, R.drawable.ic_weather_sunny_primary_black_24dp),
    ISOLATED_THUNDERSTORMS(37, R.string.condition_isolated_thunderstorms, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    SCATTERED_THUNDERSTORMS(38, R.string.condition_scattered_thunderstorms, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    SCATTERED_THUNDERSTORMS_2(39, R.string.condition_scattered_thunderstorms, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    SCATTERED_SHOWERS(40, R.string.condition_scattered_showers, R.drawable.ic_weather_pouring_primary_black_24dp),
    HEAVY_SNOW(41, R.string.condition_heavy_snow, R.drawable.ic_weather_snowy_primary_black_24dp),
    SCATTERED_SNOW_SHOWERS(42, R.string.condition_scattered_snow_showers, R.drawable.ic_weather_snowy_primary_black_24dp),
    HEAVY_SNOW_2(43, R.string.condition_heavy_snow, R.drawable.ic_weather_snowy_primary_black_24dp),
    PARTLY_CLOUDY(44, R.string.condition_partly_cloudy, R.drawable.ic_weather_partlycloudy_primary_black_24dp),
    THUNDERSHOWERS(45, R.string.condition_thundershowers, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    SNOW_SHOWERS(46, R.string.condition_snow_showers, R.drawable.ic_weather_snowy_primary_black_24dp),
    ISOLATED_THUNDERSHOWERS(47, R.string.condition_isolated_thundershowers, R.drawable.ic_weather_lightning_rainy_primary_black_24dp),
    NOT_AVAILABLE(3200, R.string.condition_not_available, R.drawable.ic_not_interested_primary_black_24dp);

    @IntRange(from = 0, to = 3200)
    private final int code;
    @StringRes
    private final int conditionId;
    @DrawableRes
    private final int conditionImageId;

    WeatherConditionCode(@IntRange(from = 0, to = 3200) int code, @StringRes int conditionId, @DrawableRes int conditionImageId) {
        this.code = code;
        this.conditionId = conditionId;
        this.conditionImageId = conditionImageId;
    }

    @IntRange(from = 0, to = 3200)
    public int getCode() {
        return code;
    }

    @StringRes
    public int getConditionId() {
        return conditionId;
    }

    @DrawableRes
    public int getConditionImageId() {
        return conditionImageId;
    }
}
