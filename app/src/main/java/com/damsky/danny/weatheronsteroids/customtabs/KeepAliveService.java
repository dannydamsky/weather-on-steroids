package com.damsky.danny.weatheronsteroids.customtabs;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * @author Sascha Peilicke
 * @see <a href="https://github.com/saschpe/android-customtabs">Android CustomTabs Library</a>
 */
public final class KeepAliveService extends Service {

    private static final Binder binder = new Binder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
