package com.damsky.danny.weatheronsteroids.data.db.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * @author Danny Damsky
 * @see WeatherLocation
 * @see Dao
 */
@Dao
public interface WeatherLocationDao {
    @Query("SELECT * FROM locations WHERE is_added = 1 ORDER BY location_name ASC")
    LiveData<List<WeatherLocation>> getAllAdded();

    @Query("SELECT * FROM locations")
    List<WeatherLocation> getAllBlocking();

    @Query("SELECT * FROM locations WHERE latitude = :latitude and longitude = :longitude")
    LiveData<WeatherLocation> getByLocation(double latitude, double longitude);

    @Query("SELECT * FROM locations WHERE id = :id")
    LiveData<WeatherLocation> getById(String id);

    @Query("SELECT * FROM locations WHERE id = :id")
    WeatherLocation getByIdBlocking(String id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(WeatherLocation weatherLocation);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(WeatherLocation... weatherLocations);

    @Query("UPDATE locations SET is_added = 0 WHERE id = :id")
    void delete(String id);

    @Query("UPDATE locations SET is_added = 0")
    void deleteAll();

    @Query("UPDATE locations SET is_added = 1 WHERE id = :id")
    void restore(String id);

    @Update
    void update(WeatherLocation weatherLocation);

    @Update
    void updateAll(List<WeatherLocation> weatherLocations);
}
