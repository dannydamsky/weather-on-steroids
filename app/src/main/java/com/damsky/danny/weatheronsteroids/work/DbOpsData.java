package com.damsky.danny.weatheronsteroids.work;

import android.support.annotation.NonNull;

import com.damsky.danny.weatheronsteroids.data.db.model.WeatherLocation;
import com.damsky.danny.weatheronsteroids.data.optimization.OptimizedWeatherLocation;
import com.damsky.danny.weatheronsteroids.util.yahoo.YahooRequestBuilder;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;

import androidx.work.Data;

/**
 * This class is used to build and parse Data objects that are unique to the DbOpsWorker class.
 * Most of the functions are package-private because the class is normally intended to be accessed
 * only by the DbOpsWorker class.
 *
 * @author Danny Damsky
 * @see DbOpsWorker
 * @see Data
 */
public final class DbOpsData {

    private static final String REQUEST_URL =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_URL";

    private static final String REQUEST_CODE =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_CODE";

    private static final String REQUEST_ID =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_ID";

    private static final String REQUEST_NAME =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_NAME";

    private static final String REQUEST_LATITUDE =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_LATITUDE";

    private static final String REQUEST_LONGITUDE =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_LONGITUDE";

    private static final String REQUEST_ADDED =
            "com.damsky.danny.weatheronsteroids.work.DbOpsWorker.REQUEST_ADDED";

    private static final int REQUEST_LATLNG_DEFAULT = -200;

    static final int REQUEST_CODE_INSERT = 152;
    static final int REQUEST_CODE_SYNC_WITH_FIREBASE = 613;
    public static final int REQUEST_CODE_UPDATE_ALL = 53;
    static final int REQUEST_CODE_UPDATE_SINGLE = 623;

    private String requestUrl;
    private final int requestCode;
    private final String requestId;
    private final String requestName;
    private final double requestLatitude;
    private final double requestLongitude;
    private final boolean requestAdded;

    DbOpsData(@NonNull Data data) {
        this.requestUrl = data.getString(REQUEST_URL);
        this.requestCode = data.getInt(REQUEST_CODE, -1);
        this.requestId = data.getString(REQUEST_ID);
        this.requestName = data.getString(REQUEST_NAME);
        this.requestLatitude = data.getDouble(REQUEST_LATITUDE, REQUEST_LATLNG_DEFAULT);
        this.requestLongitude = data.getDouble(REQUEST_LONGITUDE, REQUEST_LATLNG_DEFAULT);
        this.requestAdded = data.getBoolean(REQUEST_ADDED, false);
    }

    String getRequestUrl() {
        return requestUrl;
    }

    int getRequestCode() {
        return requestCode;
    }

    String getRequestId() {
        return requestId;
    }

    public void setRequestUrl(@NonNull WeatherLocation location) {
        requestUrl = YahooRequestBuilder.getLocationRequestUrl(location.getLatitude(), location.getLongitude());
    }

    WeatherLocation buildWeatherLocation(@NonNull String lastKnownResponse) {
        WeatherLocation.Builder builder = new WeatherLocation.Builder();
        if (requestId != null) {
            builder.setId(requestId);
        }
        return builder.setLocationName(requestName)
                .setLatitude(requestLatitude)
                .setLongitude(requestLongitude)
                .setLastKnownResponse(lastKnownResponse)
                .setAdded(requestAdded)
                .build();
    }

    @Override
    public String toString() {
        return "DbOpsData{" +
                "requestUrl='" + requestUrl + '\'' +
                ", requestCode=" + requestCode +
                ", requestId='" + requestId + '\'' +
                ", requestName='" + requestName + '\'' +
                ", requestLatitude=" + requestLatitude +
                ", requestLongitude=" + requestLongitude +
                ", requestAdded=" + requestAdded +
                '}';
    }

    public static final class Builder {
        private String requestUrl;
        private int requestCode;
        private String requestId;
        private String requestName;
        private double requestLatitude = REQUEST_LATLNG_DEFAULT;
        private double requestLongitude = REQUEST_LATLNG_DEFAULT;
        private boolean requestAdded = false;

        public Builder setRequestCode(int requestCode) {
            this.requestCode = requestCode;
            return this;
        }

        Builder setWeatherLocation(@NonNull OptimizedWeatherLocation location) {
            this.requestId = location.getId();
            return setRequestUrl(location.getLatitude(), location.getLongitude());
        }

        Builder setWeatherLocation(@NonNull WeatherLocation location) {
            this.requestId = location.getId();
            this.requestName = location.getLocationName();
            this.requestLatitude = location.getLatitude();
            this.requestLongitude = location.getLongitude();
            this.requestAdded = location.isAdded();
            return setRequestUrl(requestLatitude, requestLongitude);
        }

        Builder setPlace(@NonNull Place place, boolean shouldAdd) {
            final LatLng latLng = place.getLatLng();
            this.requestId = place.getId();
            this.requestName = place.getName().toString();
            this.requestLatitude = latLng.latitude;
            this.requestLongitude = latLng.longitude;
            this.requestAdded = shouldAdd;
            return setRequestUrl(requestLatitude, requestLongitude);
        }

        private Builder setRequestUrl(double latitude, double longitude) {
            requestUrl = YahooRequestBuilder.getLocationRequestUrl(latitude, longitude);
            return this;
        }

        public Data buildData() {
            final Data.Builder dataBuilder = new Data.Builder();
            dataBuilder.putInt(REQUEST_CODE, requestCode);
            if (requestName != null) {
                dataBuilder.putString(REQUEST_NAME, requestName);
            }
            if (requestId != null) {
                dataBuilder.putString(REQUEST_ID, requestId);
            }
            if (requestLatitude != REQUEST_LATLNG_DEFAULT && requestLongitude != REQUEST_LATLNG_DEFAULT) {
                dataBuilder.putDouble(REQUEST_LATITUDE, requestLatitude);
                dataBuilder.putDouble(REQUEST_LONGITUDE, requestLongitude);
            }
            if (requestUrl != null) {
                dataBuilder.putString(REQUEST_URL, requestUrl);
            }
            dataBuilder.putBoolean(REQUEST_ADDED, requestAdded);
            return dataBuilder.build();
        }

        @Override
        public String toString() {
            return "Builder{" +
                    "requestUrl='" + requestUrl + '\'' +
                    ", requestCode=" + requestCode +
                    ", requestId='" + requestId + '\'' +
                    ", requestName='" + requestName + '\'' +
                    ", requestLatitude=" + requestLatitude +
                    ", requestLongitude=" + requestLongitude +
                    ", requestAdded=" + requestAdded +
                    '}';
        }
    }
}
