package com.damsky.danny.weatheronsteroids;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.damsky.danny.weatheronsteroids.customtabs.CustomTabsActivityLifecycleCallbacks;
import com.damsky.danny.weatheronsteroids.data.prefs.PreferencesHelper;
import com.damsky.danny.weatheronsteroids.util.Constants;
import com.facebook.device.yearclass.YearClass;

import io.fabric.sdk.android.Fabric;


/**
 * This class is used to initialize singleton classes and constant values.
 *
 * @author Danny Damsky
 */
public final class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PreferencesHelper.initialize(this);
        Constants.DEVICE_YEAR = YearClass.get(this);
        registerActivityLifecycleCallbacks(new CustomTabsActivityLifecycleCallbacks());
    }
}
